# School Tag 

Gamifying the School Commute to improve student wellness and independence while reducing automombile traffic.  
Students _tag in_ with their [NFC](https://en.wikipedia.org/wiki/Near-field_communication) sticker at school to receive
rewards for positive commute beahvior:

* Arriving on time
* Biking
* Walking
* Carpooling
* Taking the bus

## School Tag App

A [progressive web app](https://developers.google.com/web/progressive-web-apps/) for participants and observers of School Tag to watch and play the game.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Deploying to Firebase Hosting

Run `npm run env:deploy` to deploy to the [sandbox](https://sandbox.schooltag.org) for testing and validation.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Material Design

Using [Angular Material](https://material.angular.io/) to achieve [Material Design](https://material.io/).

## Cloud

The application materializes the [School Tag Cloud](https://bitbucket.org/school-tag/school-tag-cloud) application. 
The app is hosted in the same Firebase project as the cloud application.

## Sandbox

https://sandbox.schooltag.org is the place where code first plays together.

## Playground

https://playground.schooltag.org is the place where code plays well together.

## Tournaments

In the future, each participating school will have their own firebase projects hosting the web and cloud applications.


## Free and Open Source

All source code is free and open source. See [license](LICENSE.md). 
Nominal costs of hosting on [Firebase](https://firebase.google.com/).
