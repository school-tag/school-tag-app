#!/usr/bin/env bash

ENVIRONMENT=$1
VERSION="$BITBUCKET_BUILD_NUMBER"

if [ -z "$ENVIRONMENT" ]; then
  echo "app title is required as first param"
  exit -1
fi

#Most observers will have a single school tag app.
TITLE="School Tag"

# testing benefits from different names for target environments
if [[ $ENVIRONMENT == "sandbox" ]]; then
  TITLE="Sandbox"
elif [[ $ENVIRONMENT == "playground" ]];then
  TITLE="Playground"
fi

# notice the output here needs to be referenced in index.html
# images below must be relative
cat << EOF > ./src/manifest.json
{
  "name": "$TITLE",
  "short_name": "$TITLE",
  "version": "$VERSION",
  "icons": [
    {
      "src": "assets/images/logo/android-chrome-192x192.png?v=2bQ67oAoMO",
      "sizes": "192x192",
      "type": "image/png"
    },
    {
      "src": "assets/images/logo/android-chrome-512x512.png?v=2bQ67oAoMO",
      "sizes": "512x512",
      "type": "image/png"
    }
  ],
  "lang": "en-US",
  "display": "standalone",
  "orientation": "portrait",
  "theme_color": "#FFF3D5",
  "background_color": "#ffffff",
  "start_url": "./?utm_source=web_app_manifest"
}
EOF
