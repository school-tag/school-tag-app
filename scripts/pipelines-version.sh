#!/bin/bash
# Exports the version JSON generated during a bitbucket pipelines build
# all $BITBUCKET_* come from environment variables
# https://confluence.atlassian.com/bitbucket/environment-variables-794502608.html
# copied from ... https://bitbucket.org/school-tag/school-tag-cloud/src/12176cb04529/pipelines/bin/pipelines-env-generation.sh?at=master&fileviewer=file-view-default

#usage ./pipelines-version.sh school-tag-sbox


_PROJECT=$1
_ENV=$2

_BRANCH=${BITBUCKET_BRANCH:-$(git branch | sed -n -e 's/^\* \(.*\)/\1/p')}
_COMMIT=${BITBUCKET_COMMIT:-$(git rev-parse HEAD)}
# build numb provided by ci or the number of seconds since epoch if local
_BUILD_NUMBER=${BITBUCKET_BUILD_NUMBER:-$(date +%s)}

if [ -z "$_PROJECT" ]; then
   echo "Project id is required as first parameter"
   exit 1
fi

if [ -z "$_ENV" ]; then
   echo "Environment is required as second parameter (sandbox, playground, demo, wca,etc)"
   exit 1
fi

if [ -z "$BITBUCKET_REPO_OWNER" ]; then
   baseUrl=$(git config --get remote.origin.url)
else
   baseUrl="https://bitbucket.org/$BITBUCKET_REPO_OWNER/$BITBUCKET_REPO_SLUG"
fi

projectId="$_PROJECT"
branchUrl="$baseUrl/branch/$_BRANCH"
commitUrl="$baseUrl/commits/$_COMMIT"
buildUrl="$baseUrl/addon/pipelines/home#!/results/$_BUILD_NUMBER"
selfUrl="https://${_ENV}.schooltag.org/developers"

echo -e "/* tslint:disable */"
echo -e "export const webSourceVersion ={"
echo -e "  selfUrl: '$selfUrl',"
echo -e "  branchUrl: '$branchUrl',"
echo -e "  commitUrl: '$commitUrl',"
echo -e "  buildUrl: '$buildUrl',"
echo -e "  buildNumber: '$_BUILD_NUMBER',"
echo -e "  branch: '$_BRANCH',"
echo -e "  commit: '$_COMMIT'",
echo -e "  environment: '$_ENV'",
echo -e "  projectId: '$projectId'"
echo -e "}"

