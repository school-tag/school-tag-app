import {Component} from '@angular/core';
import {Router} from "@angular/router";
import {AppRoutes} from "../../app.routes";
import {AngularFireAuth} from "angularfire2/auth";
import {UserService} from "../../users/user.service";
import {User} from "../../users/user.entity";
import {MatSnackBar} from "@angular/material";
import {UserRewardsService} from "../../game/user-rewards.service";
import {Reward} from "../../game/reward.model";
import {Observable} from 'rxjs';


@Component({
  selector: 'app-layout',
  templateUrl: './app-layout.component.html',
  styleUrls: ['./app-layout.component.css']
})
export class AppLayoutComponent{

  title = 'School Tag';
  public readonly homeSelector = AppRoutes.daySelector();
  public readonly homeIcon = AppRoutes.dayIcon;
  public readonly aboutSelector = AppRoutes.aboutSelector;
  public readonly playersSelector = AppRoutes.usersPath;
  public readonly faqSelector = AppRoutes.faqPath;
  public authenticatedUser: any;
  /**if the authenticated user is a player of the game */
  public authenticatedPlayer: User;
  public authenticatedPlayerYearRewards: Reward;

  private favorites:Observable<any>;

  /**Users that are marked as a favorite, in a consistent order */
  public favoriteUsers:Observable<User[]>;

  constructor(private router: Router,
              private angularFireAuth: AngularFireAuth,
              private userService: UserService,
              private userRewardsService: UserRewardsService,
              private snackBar: MatSnackBar) {

    this.angularFireAuth.authState.subscribe((response) => {
      if (response) {
        console.log(`Logged in as ${response.uid}`);
        this.authenticatedUser = response;
        this.userService.userByAuthId(this.authenticatedUser.uid).subscribe((user) => {
          this.authenticatedPlayer = user;
        });
      } else {
        console.log('Logged out.');
      }
    });

    //load favorites
    this.favorites = this.userService.favorites();
    this.favorites.subscribe((favorites)=>{
      if(favorites){
        const favoriteUserIds = Object.keys(favorites);
        this.favoriteUsers = this.userService.usersByIds(favoriteUserIds);
      }
    });
  }

  /**
   * Navigates to the authenticated users.
   */
  public showAuthUser() {
    //the choose avatar shows identity which should become the identity settings page
    //the avatar menu item will go to the player page.
    this.chooseAvatar();
  }

  /** chosen in the menu when clicking on the avatar associated to the authenticated user or a favorite.*/
  public showPlayer(userId:string){
    if(!userId && this.authenticatedPlayer){
      userId = this.authenticatedPlayer.id;
    }
    if(userId){
      const path = AppRoutes.userDayPath(userId);
      AppRoutes.navigate(path, this.router, this.snackBar);
    }else{
      this.chooseAvatar();
    }

  }

  /**
   * Associates an avatar to this identity...only when not already associated.
   */
  public chooseAvatar() {
    const path = AppRoutes.avatarPath;
    AppRoutes.navigate(path, this.router, this.snackBar);
  }

  public signIn() {
    AppRoutes.signIn(this.router,this.snackBar);
  }

  public signOut() {
    if (this.authenticatedUser && this.angularFireAuth) {
      this.angularFireAuth.auth.signOut().catch((error) => {
        console.log('unable to signout', error);
      });
      this.authenticatedUser = null;
      this.authenticatedPlayer = null;
    }
  }

  /**
   * @param {String} selector the path that matches the router
   */
  navigate(selector: string) {
    this.router.navigateByUrl(selector).catch((error) => {
      console.log(error);
    });
  }

  navigateToRules() {
    AppRoutes.navigate(AppRoutes.rulesPath, this.router, this.snackBar);
  }

  navigateToLeaderboard() {
    AppRoutes.navigate(AppRoutes.leaderboardPath, this.router, this.snackBar);
  }


}
