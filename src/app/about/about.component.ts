import { Component, OnInit } from '@angular/core';
import {AppRoutes} from "../app.routes";
import {Router} from '@angular/router';
import {MatSnackBar} from "@angular/material";

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  public readonly developerSelector = AppRoutes.developerSelector;
  constructor(private router: Router,private snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  viewPrivacy(){
    AppRoutes.signIn(this.router, this.snackBar);
  }
}
