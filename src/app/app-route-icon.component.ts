import {Component, Input, OnInit} from "@angular/core";
import {PointsRewardIconComponent} from "./game/reward.component";
import {AppRouteIconService} from "./app-route-icon.service";


/**
 * Locations within the app are routable using a Selector.  This associates an icon given the selector.
 */
@Component({
  selector: 'app-route-icon',
  template: '<mat-icon color="primary">{{iconName}}</mat-icon>',
  styles: PointsRewardIconComponent.iconStyles,
})
export class AppRouteIconComponent implements OnInit{
  /**
   * Path part of the URL from AppRoutes.
   */
  @Input()
  public selector:string;

  /**
   * the value in mat-icon to be displayed, set during initiation by matching from the selector
   */
  public  iconName:string;

  constructor(private appRouteIconService: AppRouteIconService){

  }

  ngOnInit(){
    this.iconName = this.appRouteIconService.iconNameForSelector(this.selector);
  }



}
