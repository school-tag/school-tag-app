import {Injectable} from "@angular/core";
import {AppRoutes} from "./app.routes";


@Injectable()
export class AppRouteIconService {

  /**
   *
   */
  private selectorIconMap;

  constructor() {
    this.selectorIconMap = {};
    this.selectorIconMap[AppRoutes.prizesPath] = 'card_giftcard';
  }

  /**
   * Provides the icon name matching the selector, as registered during construction.
   * Returns a bug icon if none found.
   * @param {string} selector
   */
  public iconNameForSelector(selector: string) {
    const iconName = this.selectorIconMap[selector];
    return (iconName) ? iconName : 'bug_report';
  }
}
