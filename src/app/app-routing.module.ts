import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PreloadAllModules, RouterModule, Routes} from "@angular/router";
import {AppRoutes} from "./app.routes";
import {AboutComponent} from "./about/about.component";
import {DeveloperComponent} from "./developer/developer.component";
import {AuthComponent} from "./auth/auth.component";
import {RulesComponent} from "./rules/rules.component";
import {LeaderboardComponent} from "./leaderboard/leaderboard.component";
import {LeaderboardsComponent} from "./leaderboard/leaderboards.component";
import {AppRouteIconComponent} from "./app-route-icon.component";
import {MatIconModule} from "@angular/material";
import {AppComponent} from './app.component';
import {DayComponent} from './day/day.component';
import {ScanComponent} from './scan/scan.component';
import {DaysComponent} from './days/days.component';
import {AppLayoutComponent} from './_layout/app-layout/app-layout.component';
import {WallboardComponent} from './wallboard/wallboard.component';
import {UsersComponent} from './users/users.component';
import {UserComponent} from './user/user.component';
import {TagComponent} from './tag/tag.component';
import {AvatarComponent} from './user/avatar.component';
import {FaqComponent} from './faq/faq.component';


export const ROUTES: Routes = [
  {
    path: '',
    component: AppLayoutComponent,
    children: [
      {path: AppRoutes.homePath, component: DayComponent},
      {path: AppRoutes.dayPathTemplate, component: DayComponent},
      {path: AppRoutes.scanSelector, component: ScanComponent},
      {path: AppRoutes.scanStationSelector, component: ScanComponent},
      {path: 'days', component: DaysComponent},
      {path: AppRoutes.rulesPath, component: RulesComponent},
      {path: AppRoutes.leaderboardPath, component: LeaderboardsComponent},
      {path: AppRoutes.usersPath, component: UsersComponent},
      {path: AppRoutes.userPathTemplate, component: UserComponent},
      {path: AppRoutes.userDayPathTemplate, component: UserComponent},
      {path: AppRoutes.tagPathTemplate, component: TagComponent},
      {path: AppRoutes.tagDayPathTemplate, component: TagComponent},
      {path: AppRoutes.avatarPath, component: AvatarComponent},
      {path: AppRoutes.signInSelector, component: AuthComponent},
      {path: AppRoutes.developerSelector, component: DeveloperComponent},
      {path: AppRoutes.aboutSelector, component: AboutComponent},
      {path: AppRoutes.faqPath, component: FaqComponent},
    ]
  },
  {
    path: 'wallboard',
    component: WallboardComponent,
  },
];

@NgModule({
  declarations: [
    AppRouteIconComponent,
  ],
  imports: [
    RouterModule.forRoot(ROUTES, {preloadingStrategy: PreloadAllModules}),
    MatIconModule,
  ],
  exports: [
    AppRouteIconComponent,
    RouterModule,
  ]
})
export class AppRoutingModule {
}
