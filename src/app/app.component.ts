import {Component} from '@angular/core';
import {Router} from "@angular/router";
import {AppRoutes} from "./app.routes";
import {AngularFireAuth} from "angularfire2/auth";
import {UserService} from "./users/user.service";
import {User} from "./users/user.entity";
import {MatSnackBar} from "@angular/material";
import {UserRewardsService} from "./game/user-rewards.service";
import {Reward} from "./game/reward.model";
import {Observable} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

}
