import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {PreloadAllModules, RouterModule} from "@angular/router";
import {DaysModule} from "./days/days.module";
import {UsersModule} from "./users/users.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MaterialModule} from "./material.module";
import {AngularFireModule} from "angularfire2";
import {environment} from "../environments/environment";
import {AngularFireDatabaseModule} from "angularfire2/database";
import {UserService} from "./users/user.service";
import {UserRewardsService} from "./game/user-rewards.service";
import {GameTimeService} from "./game/game-time.service";
import {MessageService} from "./message/message.service";
import {AvatarService} from "./game/avatar.service";
import {DeveloperComponent} from './developer/developer.component';
import {AboutComponent} from './about/about.component';
import {AppRoutingModule} from "./app-routing.module";
import {RuleIconService} from "./game/rule-icon.service";
import {AngularFireAuthModule} from "angularfire2/auth";
import {AuthMethods, AuthProvider, FirebaseUIAuthConfig, FirebaseUIModule} from "firebaseui-angular";
import {AuthComponent} from './auth/auth.component';
import {TagService} from "./tag/tag.service";
import {TagCategoryService} from "./tag/tag-category.service";
import {ImageService} from "./image/image.service";
import {AsyncLocalStorageModule} from "angular-async-local-storage";
import {RulesModule} from "./rules/rules.module";
import {LeaderboardModule} from "./leaderboard/leaderboard.module";
import {PlayerRankService} from "./leaderboard/player-rank.service";
import {FaqModule} from "./faq/faq.module";
import {AppRouteIconService} from "./app-route-icon.service";
import {StationService} from './station/station.service';
import {WallboardModule} from './wallboard/wallboard.module';
import {AppLayoutComponent} from './_layout/app-layout/app-layout.component';


// Application wide providers
//FIXME: this must come from the environment
const utcOffset = "-07:00";
export const gameTimeService = new GameTimeService(utcOffset);
export const imageService = new ImageService('https://school-tag.imgix.net')
const APP_PROVIDERS = [
  {provide: GameTimeService, useValue: gameTimeService},
  {provide: ImageService, useValue: imageService},
  AppRouteIconService,
  AvatarService,
  MessageService,
  PlayerRankService,
  RuleIconService,
  StationService,
  TagCategoryService,
  TagService,
  UserService,
  UserRewardsService,
];

// const facebookCustomConfig: AuthProviderWithCustomConfig = {
//   provider: AuthProvider.Facebook,
//   customConfig: {
//     scopes: [
//       'public_profile',
//       'email',
//       'user_likes',
//       'user_friends'
//     ],
//     customParameters: {
//       // Forces password re-entry.
//       auth_type: 'reauthenticate'
//     }
//   }
// };
//see ./auth/auth.component
const firebaseUiAuthConfig: FirebaseUIAuthConfig = {
  providers: [
    AuthProvider.Google,
    //facebookCustomConfig,
    //AuthProvider.Twitter,
    //AuthProvider.Github,
    AuthProvider.Password,
    AuthProvider.Phone,
  ],
  method: AuthMethods.Popup,
  tos: ''
};

@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    AuthComponent,
    DeveloperComponent,
    AppLayoutComponent,
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AsyncLocalStorageModule,
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    DaysModule,
    FaqModule,
    FirebaseUIModule.forRoot(firebaseUiAuthConfig),
    LeaderboardModule,
    MaterialModule,
    RulesModule,
    RouterModule.forRoot([], {preloadingStrategy: PreloadAllModules}),
    UsersModule,
    WallboardModule,
  ],
  providers: [APP_PROVIDERS],
  bootstrap: [AppComponent],

})
export class AppModule {
}
