import {ParamMap, Params, Router, Routes} from '@angular/router';
import {DayComponent} from "./day/day.component";
import {AboutComponent} from "./about/about.component";
import {DeveloperComponent} from "./developer/developer.component";
import {MatSnackBar} from "@angular/material";


const userPathKey = 'p';
const dayPathKey = 'd';
const userParamKey = 'userKey';
const dateParamKey = 'date';

/**
 * Simple declarations used to write once and reference everywhere. This is not coupled
 * with any routing mechanism to avoid cyclical dependencies.  So any component can reference
 * since this does not reference any component back.  see *routing.module.ts
 */
export class AppRoutes{

  public static readonly aboutSelector = 'about';
  public static readonly developerSelector = 'dev';
  public static readonly dayPathTemplate = `d/:${dateParamKey}`;
  public static readonly dayIcon = 'view_list';
  public static readonly homePath = '';
  public static readonly faqPath = 'faq';
  public static readonly scanSelector = 'scan';

  public static readonly tagTypeParamKey = 'tagType';
  public static readonly tagCategoryParamKey = 'tagCategory';
  public static readonly scanStationSelector = 'scan/:tagCategory/:tagType'; //scan/walk/station or scan/bike/tag


  public static readonly signInSelector = 'signIn';
  //maintainability would improve with variable reference, readability suffers
  public static readonly userPathTemplate = 'p/:userKey';
  public static readonly usersPath = 'users';
  public static readonly avatarPath = 'avatar';
  public static readonly userDayPathTemplate = 'p/:userKey/d/:date';
  public static readonly tagIdKey='tagId';
  public static readonly tagPathTemplate = 'tag/:tagId';
  public static readonly tagDayPathTemplate = `${AppRoutes.tagPathTemplate}/${AppRoutes.dayPathTemplate}`;

  public static readonly rulesPath = 'rules';
  public static readonly prizesPath = 'prizes';


  public static readonly leaderboardPath = 'leaderboard';

  public static readonly wallboardPath = 'wallboard';

  static userKeyParam(params: Params) {
    return params[userParamKey]
  }


  static dayParamFromMap(params:ParamMap){
    return params.get(dateParamKey);
  }
  static dayParam(params: Params){
    return params[dateParamKey];
  }

  /**
   *
   * @returns {string} used by router to navigate to a day
   */
  public static daySelector(date?: string) {
    if (date) {
      return `d/${date}`;
    } else {
      //currently the home page, no date shows the latest match
      return AppRoutes.homePath;
    }
  }

  /**
   *
   * @param {string} tagId
   * @param {string} day, if provided, will jump to the scans for that tag on the day
   * @return {string} the path the router will use
   */
  public static tag(tagId:string,day ?:string){
    if(day){
      return `tag/${tagId}/d/${day}`;
    }
    return `tag/${tagId}`;
  }

  /**Views the user on the date given.
   *
   * @param {string} userKey userId or nickname
   * @param {string} [date] optionally an iso date
   * @returns {string} the routePath that can be used by the navigator.
   */
  static userDayPath(userKey: string, date ?: string) {

    let result = `p/${userKey}`;
    if (date) {
      result = `${result}/d/${date}`
    }
    return result;
  }

  /**
   * Common method to navigate by url and handle any error.
   * @param {string} path where to navigate (from this class typicallY)
   * @param {Router} router for navigation
   * @param {MatSnackBar} [snackBar] for error reporting
   */
  static navigate(path: string, router: Router, snackBar?: MatSnackBar) {
    router.navigateByUrl(path).catch((error)=>{
      if(snackBar){
        snackBar.open('Failed to show page','OK');
      }
      console.error(`failed to navigate to ${path}`,error);
    });
  }

  static signIn(router:Router,snackBar?: MatSnackBar) {
    AppRoutes.navigate(AppRoutes.signInSelector,router,snackBar);
  }
}
