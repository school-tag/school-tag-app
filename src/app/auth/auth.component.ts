import {Component, OnInit} from '@angular/core';
import {FirebaseUISignInSuccess} from "firebaseui-angular";
import {Router} from "@angular/router";
import {AppRoutes} from "../app.routes";
import {MatSnackBar} from "@angular/material";

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  public oldEnough:boolean;
  public under13:boolean;

  constructor(private router: Router,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
  }

  successCallback(signInSuccessData: FirebaseUISignInSuccess) {
    AppRoutes.navigate(AppRoutes.avatarPath,this.router,this.snackBar);
  }

  oldEnoughChosen(){
    this.oldEnough = true;
  }

  under13Chosen(){
    this.under13 = true;
  }
}
