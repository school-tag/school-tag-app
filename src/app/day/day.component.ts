import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Location} from '@angular/common';
import {Message} from "../message/message.entity";
import {MessageService} from "../message/message.service";
import {AvatarService} from "../game/avatar.service";
import {AppRoutes} from "../app.routes";
import {GameTimeService} from "../game/game-time.service";
import * as moment from "moment";
import {Observable} from "rxjs/Observable";
import {Subscription} from "rxjs/Subscription";
import {DomSanitizer} from "@angular/platform-browser";
import {gameTimeService} from '../app.module';

/**Highlights the achievements for all players for a single day.
 *
 */
@Component({
  selector: 'app-day',
  templateUrl: './day.component.html',
  styleUrls: ['./day.component.css']
})
export class DayComponent implements OnInit, OnDestroy {

  /** the message stream of achievements for the day.*/
  public messages: Observable<Message[]>;
  /**
   *
   */
  public day: string;

  /**
   * Used to report the day or countdown to school start.
   */
  public schoolStartTime: moment.Moment;
  public gameStartTime: moment.Moment;
  public gameIsUpcoming: boolean;
  public gameIsActive: boolean;
  public gameIsPast: boolean;

  /**Describes the day being viewed.*/
  public nextDayIsAvailable = false;
  private staleDayTimer: Subscription;


  constructor(private route: ActivatedRoute,
              private router: Router,
              private location: Location,
              private messageService: MessageService,
              private avatarService: AvatarService,
              private gameTimeService: GameTimeService,
              private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.setDay(AppRoutes.dayParam(params));
    })
  }

  ngOnDestroy() {
    this.cancelStaleDayTimer();
  }


  /**
   * shows the day given
   *
   * @param {string} [day] in iso format. if not provided, today is used
   */
  setDay(day?) {
    day = this.gameTimeService.dayFromParam(day);
    this.day = day;
    this.schoolStartTime = this.gameTimeService.parseDay(day);
    this.gameStartTime = this.gameTimeService.parseDay(day);
    //FIXME:start time needs to be provided by the game time service...and configured
    const state = this.gameTimeService.gameState();
    if(this.isToday()){
      this.schoolStartTime = state.schoolStartTime;
      this.gameStartTime = state.gameStartTime;
    }
    this.nextDayIsAvailable = this.gameTimeService.nextDayIsAvailable(day);

    //update the browser address
    this.location.go(AppRoutes.daySelector(this.day));

    this.messages = this.messageService.messagesForDay(this.day);
    this.updateActiveState();
    this.startRefreshTimer();
  }



  today() {
    this.setDay();
  }

  isToday() {
    return this.gameTimeService.today() === this.day;
  }

  nextDay() {
    this.setDay(this.gameTimeService.nextDay(this.day));
  }

  previousDay() {
    this.setDay(this.gameTimeService.previousDay(this.day));
  }

  viewUser(userId: string) {
    return this.router.navigateByUrl(AppRoutes.userDayPath(userId, this.day));
  }

  /**
   * Refreshes the state of the game time relative states compared to now.
   */
  updateActiveState(){
    this.gameIsUpcoming = this.gameTimeService.now().isBefore(this.gameStartTime);
    this.gameIsActive = this.gameTimeService.now().isBetween(this.gameStartTime,this.schoolStartTime);
    this.gameIsPast = this.gameTimeService.now().isAfter(this.schoolStartTime);
  }
  /**
   * Many parents use the same browser window to view the results. If a new day has occurred when viewing today
   * then we should update to the new day automatically.  This allows a single page to always show the latest.
   *
   */
  private startRefreshTimer() {
    if (this.isToday() && !this.staleDayTimer) {
      let futureMoment;

      if(this.gameIsUpcoming){
        futureMoment = this.gameStartTime;
      }else if(this.gameIsActive){
        futureMoment = this.schoolStartTime;
      }else{
        futureMoment = moment(this.schoolStartTime).endOf('day');
      }
      const durationTilRefresh = moment.duration(futureMoment.diff(this.gameTimeService.now())).add(moment.duration(500,'ms'));
      console.log(`refreshing in ${durationTilRefresh.toISOString()}`);
      this.staleDayTimer = Observable.timer(durationTilRefresh.asMilliseconds()).subscribe((tick) => {
        this.updateActiveState();
        console.log(`${tick} times checked for stale day`);
        if(this.gameIsPast &&  this.gameTimeService.now().dayOfYear() - this.schoolStartTime.dayOfYear() ==1){
          //calling set day will set to today and update the page and url location
          this.setDay();
        }else{
          this.staleDayTimer = null;
          this.startRefreshTimer();
        }
      });
    }
  }

  /**
   * If the stale day timer was keeping us fresh then this will cancel to avoid unwanted navigtion.
   */
  private cancelStaleDayTimer() {
    if (this.staleDayTimer) {
      console.log('canceling timer');
      this.staleDayTimer.unsubscribe();
    }
  }

  /**
   *
   * @param message
   * @returns {ImageUrl}
   */
  avatarImageUrl(message: Message) {

    return this.avatarService.imageUrl(message.aboutUser.avatar);
  }

  /**
   *
   * @param {Message} message
   * @return {string} ready to be included in css background-image
   */
  backgroundImageUrl(message: Message){
    const url = this.avatarImageUrl(message).width(200).height(200).url();
    return this.sanitizer.bypassSecurityTrustStyle(`url('${url}')`);
  }

}
