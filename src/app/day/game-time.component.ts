import {Component, EventEmitter, Output} from '@angular/core';
import {RuleIconService} from '../game/rule-icon.service';
import {GameState, GameTimeService} from '../game/game-time.service';


/**
 * Displays relative time to important moments.
 */
@Component({
  selector: 'game-state',
  template: 'School Tag {{prefix}} {{time}}',
})
export class GameTimeComponent {

  //displays relative time, per moment.to or moment.from
  public time = "";
  //starts, ends, finished, etc.
  public prefix = "calculating time...";
  public gameState:GameState;
  @Output() updated = new EventEmitter<GameState>();

  @Output() gameStarted = new EventEmitter<boolean>();
  @Output() bellRang = new EventEmitter<boolean>();
  @Output() gamedFinished = new EventEmitter<boolean>();

  /**
   *
   * @param gameTimeService to get the updates to the game state.
   */
  constructor(private gameTimeService:GameTimeService){
    this.gameTimeService.gameTimer().subscribe((updatedGameState)=>{

      if(this.gameState){
        //emit events if state changes
        if(this.gameState.upcoming && updatedGameState.active){
          this.gameStarted.emit(true);
        }else if(this.gameState.active){
          if(updatedGameState.past) {
            this.gamedFinished.emit(true);
          }
          if(!this.gameState.nearTheEnd && updatedGameState.nearTheEnd){
            this.bellRang.emit(true);
          }
        }
      }
      this.updated.emit(updatedGameState);

      //update the display message
      this.gameState = updatedGameState;
      if(this.gameState.notPlaying){
          this.time = undefined;
          this.prefix = "is not playing today"
      }else{
        const now = this.gameTimeService.now();
        if(updatedGameState.past){
          this.prefix = "finished";
          this.time = updatedGameState.schoolStartTime.from(now);
        }else if(updatedGameState.active){
          this.prefix = "ends";
          this.time = now.to(updatedGameState.schoolStartTime);
        }else{
          this.time = now.to(updatedGameState.gameStartTime);
          this.prefix = "starts";
        }
      }

    })
  }


}
