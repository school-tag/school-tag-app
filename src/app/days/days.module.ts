import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DayComponent} from '../day/day.component';
import {DaysComponent} from './days.component';
import {MaterialModule} from "../material.module";
import {MomentModule} from "angular2-moment";
import {ScanComponent} from '../scan/scan.component';
import {ScanService} from '../scan/scan.service';
import {GameDayPipe} from "../game/game-day.pipe";
import {MatGridListModule} from "@angular/material";
import {RulesModule} from "../rules/rules.module";
import {GameTimeComponent} from '../day/game-time.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    MomentModule,
    MatGridListModule,
    RulesModule,
  ],
  declarations: [
    DayComponent,
    DaysComponent,
    GameDayPipe,
    GameTimeComponent,
    ScanComponent,
  ],
  providers: [ScanService],
  exports:[
    GameDayPipe,
    GameTimeComponent,
  ]
})
export class DaysModule { }
