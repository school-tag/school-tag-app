import {Component, OnInit} from '@angular/core';
import {webSourceVersion} from "../../assets/generated/web-source-version";

@Component({
  selector: 'app-developer',
  templateUrl: './developer.component.html',
  styleUrls: ['./developer.component.css']
})
export class DeveloperComponent implements OnInit {

  public environmentInfo: any;

  constructor() {
  }

  ngOnInit() {
    this.environmentInfo = webSourceVersion;
  }

}
