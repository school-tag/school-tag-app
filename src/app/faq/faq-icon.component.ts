import {Component} from '@angular/core';

@Component({
  selector: 'st-faq-icon',
  template: '<mat-icon>question_answer</mat-icon>',
  styles: [],
})
export class FaqIconComponent {

  constructor() {
  }

}
