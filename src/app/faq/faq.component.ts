import {Component, Inject, OnInit} from '@angular/core';
import {Observable} from "rxjs/Observable";
import {Faq, FaqLink} from "./faq.entity";
import {FaqService} from "./faq.service";
import {MatSnackBar} from "@angular/material";
import {Router} from "@angular/router";
import {AppRoutes} from "../app.routes";

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})
export class FaqComponent implements OnInit {


  public  faqs:Observable<Faq[]>;

  constructor(private faqService:FaqService,
              private snackBar: MatSnackBar,
              private router: Router) { }

  ngOnInit() {
    this.faqs = this.faqService.faqs();
  }

  /**
   * When chosen, this will view the destination of the link relative or aboslute.
   * @param {FaqLink} link
   */
  public viewLink(link:FaqLink){
    if(link.selector){
      AppRoutes.navigate(link.selector,this.router,this.snackBar);
    }
    if(link.url){
      window.location.href  = link.url;
    }
  }
}
