/**
 * Frequently Asked questions, this holds the data necessary to display FAQ related to the game.
 *
 */
export interface Faq {
  /**
   * Displayed to the viewer, that indicates the category of Question/Answer.
   * Ideally one word, short and Title Case. Privacy, Rewards, Rules, etc.
   */
  subject: string;

  /** Short question displayed to get the jist of the question.*/
  question: string;

  /** complete answer displayed to the user. No formatting and all in one paragraph.*/
  answer: string;

  /** optional links to more information.*/
  links: FaqLink[];

}

export interface FaqLink{

  url:string;
  selector: string;
  icon:string;
  title:string;
}
