import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FaqComponent} from './faq.component';
import {FaqService} from "./faq.service";
import {MatButtonModule, MatExpansionModule, MatIconModule, MatToolbarModule} from "@angular/material";
import {FaqIconComponent} from "./faq-icon.component";

@NgModule({
  imports: [
    CommonModule,
    MatIconModule,
    MatExpansionModule,
    MatToolbarModule,
    MatButtonModule,
  ],
  declarations: [
    FaqComponent,
    FaqIconComponent,
  ],
  providers: [FaqService],
  exports: [MatIconModule,
    FaqIconComponent,
  ]
})
export class FaqModule {
}
