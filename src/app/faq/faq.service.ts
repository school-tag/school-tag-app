import {Injectable} from "@angular/core";
import {AngularFireDatabase} from "angularfire2/database";
import {Observable} from "rxjs/Observable";
import {User} from "../users/user.entity";
import {Faq} from "./faq.entity";


@Injectable()
export class FaqService {

  constructor(private db: AngularFireDatabase) {
  }


  /**
   *
   * @return {Observable<Faq[]>} the list of faqs, ordered by key manually created by author to force order.
   */
  public faqs() {
    return <Observable<Faq[]>> this.db.list('/faqs', (ref) => {
      return ref.orderByChild('subject');
    }).valueChanges();
  }
}
