import {Pipe, PipeTransform} from "@angular/core";
import {AvatarService} from "./avatar.service";
import {ImageUrl} from "../image/image-url.model";
import {User} from "../users/user.entity";
import {Observable} from "rxjs/Observable";

/**
 * Provides the image url related to the given user for visual identity.
 */
@Pipe({name: 'authUidAvatarImageUrl'})
export class AuthUidAvatarImageUrlPipe implements PipeTransform {
  constructor(public avatarService: AvatarService) {
  }

  transform(authUid:string): Observable<ImageUrl> {
    return  this.avatarService.authUserIdAvatarImageUrl(authUid);
  }
}
