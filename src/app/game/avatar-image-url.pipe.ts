import {ImageUrl} from "../image/image-url.model";
import {Pipe, PipeTransform} from "@angular/core";
import {AvatarService} from "./avatar.service";

/**
 * When the avatar path is already know, use this pipe to display the image.
 * <img src='{{user.avatar | avatarImageUrl}}'/>
 */
@Pipe({name: 'avatarImageUrl'})
export class AvatarImageUrlPipe implements PipeTransform {
  constructor(public avatarService: AvatarService) {
  }

  transform(avatarPath:string): ImageUrl {
    return  this.avatarService.imageUrl(avatarPath);
  }
}
