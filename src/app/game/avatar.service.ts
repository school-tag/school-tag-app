import {Injectable} from "@angular/core";
import {ImageService} from "../image/image.service";
import {ImageUrl} from "../image/image-url.model";
import {UserService} from "../users/user.service";
import {Observable} from "rxjs/Observable";
import {User} from "../users/user.entity";
import {AngularFireDatabase} from "angularfire2/database";

/**
 * Access to display and list the variety of icons that represent a user.
 */
@Injectable()
export class AvatarService {

  private defaultAvatar:ImageUrl;

  constructor(private userService: UserService,
              private imageService: ImageService,
              private db: AngularFireDatabase) {
    //currently a brown monster with question mark eyes.  using school tag logo confuses with the station manager
    this.defaultAvatar =  this.imageService.withPath('/question-mark.png');
  }


  /**
   *
   * @param {string} relativePath to the avatar, or the default if none provided
   * @returns {string}
   */
  imageUrl(relativePath?: string): ImageUrl {
    if (!relativePath) {
      return this.defaultAvatar;
    }
    return this.imageService.withPath(`/avatars/${relativePath}`);
  }

  /**
   * given the user id, this will return the imageurl that references the avatar image on the web.
   * @param {string} userId
   * @return {Observable<ImageUrl>}
   */
  public userIdAvatarImageUrl(userId: string): Observable<ImageUrl> {
    return Observable.create((observer) => {
      this.userService.userById(userId).subscribe((user) => {
        observer.next(this.userAvatarImageUrl(user));
      })
    })
  }

  public authUserIdAvatarImageUrl(authUserId: string){
    return Observable.create((observer) => {
      if(authUserId){
        this.userService.userByAuthId(authUserId).subscribe((user) => {
          observer.next(this.userAvatarImageUrl(user));
        })
      }else{
        observer.next(this.userAvatarImageUrl());
      }
    })
  }


  public useIdentity(identity:Identity){

  }
  /**
   * @param {User} user with the avatar path
   * @return {ImageUrl} the imageurl that represents the user...guaranteed since it will provide a default if no avatar
   */
  public userAvatarImageUrl(user?: User): ImageUrl {
    let imageUrl;
    if (user && user.avatar) {
      imageUrl = this.imageUrl(user.avatar);
    } else {
      imageUrl = this.imageUrl();
    }
    return imageUrl;
  }

  /**
   * Provides an identity that is not currently used by another that represents a non player, but likely a parent
   * who will give rewards.
   */
  public nonPlayerAvatarSuggestions(){
    return <Observable<Identity[]>> this.db.list('/identities', (ref) => {
      return ref.orderByChild('type').equalTo('observer');
    }).valueChanges();
  }
}
