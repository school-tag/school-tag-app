import {Pipe} from "@angular/core";
import {GameTimeService} from "./game-time.service";
import * as moment from "moment";


/** Friendly display of the day and time.
 *
 * 2017-10-04T08:15:03-08:00 | Today 8:15 am
 *
 */
@Pipe({name: 'gameDayTime'})
export class GameDayTimePipe{
  constructor(private gameTimeService:GameTimeService){

  }

  transform(moment: string|moment.Moment): string {
    return this.gameTimeService.dayTimeDisplay(moment);
  }
}
