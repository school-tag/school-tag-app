import {Pipe} from "@angular/core";
import {GameTimeService} from "./game-time.service";
import * as moment from "moment";


/** Friendly display of the day relative to now.
 *
 * day | gameDay
 * 2017-10-04 | Yesterday
 *
 */
@Pipe({name: 'gameDay'})
export class GameDayPipe{
  constructor(private gameTimeService:GameTimeService){

  }

  transform(dayString: string|moment.Moment): string {
    if(!(typeof(dayString) === 'string') ){
      dayString = this.gameTimeService.day(dayString)
    }
    return this.gameTimeService.dayDisplay(dayString);
  }
}
