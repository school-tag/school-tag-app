import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {GameState, GameTimes, GameTimeService, Time} from "./game-time.service";

describe('GameTimeService', () => {


  const thursday = "2017-11-02";
  const friday = "2017-11-03";
  const fridayNextWeek = "2017-11-10";
  const saturday = "2017-11-04";
  const sunday = "2017-11-05";
  const monday = "2017-11-06";
  const mondayNextWeek = "2017-11-13";
  const overrideSuffix = "!";
  const saturdayOverride = `${saturday}${overrideSuffix}`;
  const utcOffset = "-02:00";
  let service: GameTimeService;

  beforeEach(() => {
    service = new GameTimeService(utcOffset);
  });
  let mockNow = function (dateString: string) {
    spyOn(service, 'now').and.callFake(() => {
      return service.parseTimestamp(dateString);
    });
  };


  describe('isSchoolDay', () => {
    let assertIsSchoolDay = function (day, expected) {
      let isSchoolDay = service.isSchoolDay(day);
      return expect(isSchoolDay).toBe(expected);
    };
    it('should be false for Saturday', () => {
      return assertIsSchoolDay(saturday, false);
    });
    it('should be false for Sunday', () => {
      return assertIsSchoolDay(sunday, false);
    });
    it('should be true for Monday', () => {
      return assertIsSchoolDay(monday, true);
    });
    it('should be true for Friday', () => {
      return assertIsSchoolDay(friday, true);
    });
  });

  describe('isNextDayAvailable', () => {
    let assertNextDayIsAvailableYesterday = function (nowDateString: string, expected: boolean) {
      mockNow(nowDateString);
      const today = service.today();
      expect(today).toBe(nowDateString);
      let yesterday = service.previousDay(today);
      expect(yesterday).not.toBe(today);
      const isAvailable = service.nextDayIsAvailable(yesterday);
      return expect(isAvailable).toBe(expected);
    };
    it('should be true for yesterday on friday', () => {
      return assertNextDayIsAvailableYesterday(friday, true);
    });
    it('should be false for yesterday on saturday', () => {
      return assertNextDayIsAvailableYesterday(saturday, false);
    });
    it('should be false for yesterday on sunday', () => {
      return assertNextDayIsAvailableYesterday(sunday, false);
    });
    it('should be true for yesterday on monday', () => {
      return assertNextDayIsAvailableYesterday(monday, true);
    });
  });
  describe('mostRecentDay', () => {
    let assertMostRecentDay = function (dateString: string, expectedDateString: string) {
      const recentDateString = service.mostRecentDay(dateString);
      expect(recentDateString).toBe(expectedDateString);
    };
    it('should be friday for friday', () => {
      assertMostRecentDay(friday, friday);
    });
    it('should be friday for saturday', () => {
      assertMostRecentDay(saturday, friday);
    });
    it('should be friday for sunday', () => {
      assertMostRecentDay(sunday, friday);
    });
    it('should be monday for monday', () => {
      assertMostRecentDay(monday, monday);
    });
    it('should be no later than today', () => {
      mockNow(thursday);
      assertMostRecentDay(friday, thursday);
    });
    it('should today if not provided', () => {
      mockNow(thursday);
      assertMostRecentDay(null, thursday);
    });
  });
  describe('sameWeekdayNextWeek', () => {
    let assertSameWeekdayNextWeek = function (dateString: string, expectedDateString: string) {
      const recentDateString = service.sameWeekdayNextWeek(dateString);
      expect(recentDateString).toBe(expectedDateString);
    };
    it('should be next friday for friday', () => {
      assertSameWeekdayNextWeek(friday, fridayNextWeek);
    });
    it('should be next friday for saturday', () => {
      assertSameWeekdayNextWeek(saturday, fridayNextWeek);
    });
    it('should be next friday for sunday', () => {
      assertSameWeekdayNextWeek(sunday, fridayNextWeek);
    });
    it('should be next monday for monday', () => {
      assertSameWeekdayNextWeek(monday, mondayNextWeek);
    });
    it('should be no later than today', () => {
      mockNow(thursday);
      assertSameWeekdayNextWeek(friday, thursday);
    });
    it('should today if not provided', () => {
      mockNow(thursday);
      assertSameWeekdayNextWeek(null, thursday);
    });
  });


  describe('dayParam', () => {
    let assertDayParam = function (day, expected) {
      let isSchoolDay = service.dayFromParam(day);
      return expect(isSchoolDay).toBe(expected);
    };
    it('should return friday for Saturday', () => {
      return assertDayParam(saturday, friday);
    });
    it('should return friday for Friday', () => {
      return assertDayParam(friday, friday);
    });
    it('should return saturday for saturday override', () => {
      return assertDayParam(saturdayOverride, saturday);
    });
  });
  describe('time', () => {
    const friday815 = `${friday}T08:15:00${utcOffset}`;

    describe('timeDisplay', () => {
      it('should show 8:15 am', () => {
        return expect(service.timeDisplay(friday815)).toEqual('8:15 am');
      });
      it('should show 8:15 am', () => {
        mockNow(friday);
        return expect(service.dayTimeDisplay(friday815)).toEqual('Today 8:15 am');
      });

    });
  });
  describe('GameState', () => {
    const gameStartTime = new Time(2,0);
    const bellRingTime = new Time(2,30);
    const schoolStartTime = new Time(3,0);
    const gameTimes = new GameTimes(gameStartTime,bellRingTime,schoolStartTime);
    describe('constructor', () => {
      it('be upcoming when now is before game start time', () => {
        const before = service.now().hour(gameStartTime.hour-1).minute(gameStartTime.minute);
        const state = new GameState(gameTimes,before,false);
        expect(state.upcoming).toBe(true);
        expect(state.active).toBe(false);
        expect(state.past).toBe(false);
        expect(state.notPlaying).toBe(false);
        expect(state.nearTheEnd).toBe(false);
      });
    });
  });
});
