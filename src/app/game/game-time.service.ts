import {Injectable} from "@angular/core";
import * as moment from "moment";
import {BehaviorSubject, Observable, Observer, Subscription} from 'rxjs';
import {Duration} from 'moment';

const dayUnit = 'day';
const weekUnit = 'week';

/** Knowing the time zone of the game, this service must be used
 * for all creation/modification of moments.
 *
 * day - string represents a day in iso format
 * date - a moment with only the date that is relevant
 * timestamp - a moment where the date, time and time zone are relevant
 *
 * Day is a string, rather than a moment, to avoid time zone issues that can arise with using moment.
 */
@Injectable()
export class GameTimeService {
  //would be better to use named time zone, but not worth moment time zone library import
  private utcOffset: string;
  /**A timer that will be called at variable rates to show appropriate refresh of game time live in the display.*/
  private readonly gameStateTimer: Observable<GameState>;
  private gameStateTimerObserver: Observer<GameState>;
  private gameTimerSubscription: Subscription;
  private readonly gameTimes: GameTimes;

  constructor(utcOffset: string) {
    this.utcOffset = utcOffset;
    this.gameStateTimer = Observable.create((observer: Observer<GameState>) => {
      this.gameStateTimerObserver = observer;
      this.updateGameState();//call once to get it loaded immediately
    });
    //FIXME: this should come from the configurations, or perhaps database
    this.gameTimes = new GameTimes(new Time(7, 30), new Time(8, 35), new Time(8, 40));
    this.startRefreshTimer();
  }

  /**
   * Now, as it relates to the time zone of the game, not the time zone of the viewer.
   *
   * @returns {moment.Moment}
   */
  public now() {
    return moment().utcOffset(this.utcOffset);
  }

  /**
   * @returns {string} today's date as iso string
   */
  public today() {
    return this.day(this.now());
  }

  /**
   * @returns {number} the year from now
   */
  public thisYear() {
    return this.now().year();
  }

  public thisWeek(){
    return this.week(this.now());
  }

  /**
   * @param {moment.Moment} date
   * @returns {string} the iso formatted date string, in game time
   */
  public day(date: moment.Moment) {
    return date.format('YYYY-MM-DD');
  }

  /**
   * @param {string} day
   * @returns {number} week of the year
   */
  public weekFromDay(day: string) {
    return this.parseDay(day).isoWeek();
  }

  /**
   *
   * @param {moment.Moment} moment
   * @return {string} of the exact moment, in iso format.
   */
  public timestampString(moment: moment.Moment) {
    return moment.format();
  }

  /**
   *
   * @param {moment.Moment} [dateString] optionally the moment, otherwise returns now
   * @returns {moment.Moment}
   */
  parseTimestamp(dateString?: string):moment.Moment {
    return (dateString) ? moment.parseZone(dateString) : this.now();
  }

  /**
   *
   * @param {string} dateString
   * @returns {moment.Moment} the start of the day indicated by the string, in game time zone
   */
  parseDay(dateString?: string) {
    let result;
    if (dateString) {
      //keepLocalTime=true ensures the date will remain even as the times change.
      result = moment(dateString).utcOffset(this.utcOffset, true);
    } else {
      result = this.now();
    }
    return this.startOfDate(result);
  }


  /**
   * @param {moment.Moment} date
   * @returns {moment.Moment} of the same date and time zone, but the morning at 00:00:00.000
   */
  startOfDate(date: moment.Moment) {
    return date.startOf(dayUnit);
  }

  /**
   * @param {string} day
   * @returns {string} yesterday or the previous school day
   */
  public previousDay(day: string) {
    let date = this.parseDay(day);
    do {
      date = moment(date).subtract(1, dayUnit);
    } while (!this.isSchoolDay(this.day(date)));
    return this.day(date);
  }

  /**
   * @param {string} day in iso format
   * @returns {string} tomorrow or the next school day
   */
  public nextDay(day: string) {
    let date = this.parseDay(day);
    do {
      date = date.add(1, dayUnit);
      day = this.day(date);
    } while (!this.isSchoolDay(day));
    return day;
  }

  /**
   * @param {moment.Moment} day
   * @returns {string} the same day the previous week
   */
  public sameWeekdayPreviousWeek(day: string) {
    return this.day(this.parseDay(day).subtract(1, weekUnit));
  }

  /**
   * @param {string} day
   * @returns {string} the same day next week, or the most recent day if in the future or not a school day
   */
  public sameWeekdayNextWeek(day: string) {
    return this.mostRecentDay(this.day(this.parseDay(day).add(1, weekUnit)));
  }

  /**Uses basic weekday check to see if
   *
   * @param {string} day in iso format
   * @returns {boolean} true if this.day is a school day
   */
  public isSchoolDay(day: string) {
    const moment = this.parseDay(day);
    return moment.isoWeekday() < 6;
  }

  /**
   * @param {string} day
   * @returns {boolean} true if the day after that given is in the future and a school day
   */
  public nextDayIsAvailable(day: string) {
    return this.nextDay(day) <= this.today();
  }

  /**
   * @param {string} day
   * @returns {number} of days before now (0 is today, negative is the future)
   */
  private daysAgo(day: string) {
    const date = this.parseDay(day);
    return this.now().dayOfYear() - date.dayOfYear();
  }

  /**
   *
   * @param {string} day iso day to be converted into more friendly day description
   * @returns {string} to display to the user describing the day
   */
  public dayDisplay(day: string) {
    const daysAgo = this.daysAgo(day);
    let display;
    if (daysAgo == 0) {
      display = 'Today';
    } else if (daysAgo == 1) {
      display = 'Yesterday';
    } else {
      let format: string;
      let weeksAgo = this.weeksAgo(day);
      if (weeksAgo == 0) {
        format = 'dddd';
      } else if (weeksAgo == 1) {
        format = '[Last] dddd';
      } else {
        format = 'dddd M/DD';
      }
      display = this.parseDay(day).format(format);
    }
    return display;
  }

  /** Given a moment, this will display the time of day, in game time, with no seconds.*/
  public timeDisplay(moment: moment.Moment | string) {
    if (typeof (moment) === 'string') {
      moment = this.parseTimestamp(moment);
    }
    return this.inGameTime(moment).format('h:mm a');
  }

  public dayTimeDisplay(moment: moment.Moment | string) {
    if (typeof (moment) === 'string') {
      moment = this.parseTimestamp(moment);
    }
    const day = this.dayDisplay(this.day(moment));
    const time = this.timeDisplay(moment);
    return `${day} ${time}`;
  }

  /**
   * @param {string} day
   * @returns {boolean}
   */
  public nextWeekIsAvailable(day: string) {
    return this.weeksAgo(day) > 0;
  }

  /**
   * @param {string} day
   * @returns {number}
   */
  public weeksAgo(day: string) {
    return this.now().isoWeek() - this.parseDay(day).isoWeek();
  }

  /**
   *
   * @param {string} day the iso day that lands on the week of interest
   * @returns {string} to be displayed describing a week
   */
  public weekDisplay(day: string) {
    const weeksAgo = this.weeksAgo(day);
    if (weeksAgo == 0) {
      return 'This Week';
    } else if (weeksAgo == 1) {
      return 'Last Week';
    } else {
      return `${weeksAgo} Weeks Ago`;
    }
  }

  /** Given a date, it will find the most recent school day
   * matching the date or before.
   *
   * @param day iso date of interest
   * @return {string} of the most recent school day including today
   */
  mostRecentDay(day?: string) {

    const today = this.today();
    let mostRecent;
    if (day && day <= today) {
      mostRecent = day;
    } else {
      mostRecent = today;
    }

    while (!this.isSchoolDay(mostRecent)) {
      mostRecent = this.previousDay(mostRecent);
    }
    return mostRecent;
  }

  /**
   *  common place to adapt the moment to the game time zone
   * @param {moment.Moment} moment
   * @returns {moment.Moment}
   */
  public inGameTime(moment: moment.Moment) {
    return moment.utcOffset(this.utcOffset);
  }

  /**
   * The day given from parameters which is typically the iso date, but has some restrictions.
   *
   * 1. uses the most recentDay (school days, etc)
   * 2. dates ending in ! will use the literal date.
   *
   * @param {string} dayParam
   * @returns {string} the day as given by the param
   */
  dayFromParam(dayParam: string) {
    let day: string;
    const forceIndicator = "!";
    if (dayParam && dayParam.endsWith(forceIndicator)) {
      //forces date without being smart..useful for viewing weekends and holidays
      //this would be better with a query parameter so other days could be viewed ?auto=off
      day = dayParam.replace(forceIndicator, "");
    } else {
      day = this.mostRecentDay(dayParam);
    }
    return day;
  }

  year(moment: moment.Moment): number {
    return moment.year();
  }

  week(moment: moment.Moment): number {
    return moment.isoWeek();
  }

  dayOfWeek(moment: moment.Moment): number {
    return moment.isoWeekday();
  }

  /**
   * For those interested in updates of the game state, this will refresh at periodic
   */
  public gameTimer(): Observable<GameState> {
    return this.gameStateTimer;
  }


  private startRefreshTimer(): void {
    //FIXME: refresh at variable rate based on the game time
    this.gameTimerSubscription = Observable.timer(15 * 1000).subscribe((tick) => {
      this.updateGameState();
      //keep the timer going
      this.startRefreshTimer();
    });
  }

  private updateGameState() {
    if(this.gameStateTimerObserver){
      const gameState = this.gameState();
      this.gameStateTimerObserver.next(gameState);
    }
  }

  /**
   * @return the updated game state compared to now.
   */
  public gameState() {
    return new GameState(this.gameTimes, this.now(), false);
  }
}

export class Time {
  public readonly hour: number;
  public readonly minute: number;
  public readonly second = 0;
  public readonly millisecond = 0;

  constructor(hour, minute) {
    this.hour = hour;
    this.minute = minute;
  }

}

/**The important times of the day where the game is played and school begins.
 * Now is passed in to the methods so the times always adapt to today...which is how time is displayed.
 *
 * */
export class GameTimes {
  public readonly schoolStart: Time;
  public readonly bellRing: Time;
  private readonly gameStart: Time;

  constructor(gameStartTime: Time,
              bellRingTime: Time,
              schoolStartTime: Time
  ) {
    this.gameStart = gameStartTime;
    this.bellRing = bellRingTime;
    this.schoolStart = schoolStartTime;
  }

  private static time(now: moment.Moment, time: Time): moment.Moment {
    const moment1 = moment(now);
    moment1.hour(time.hour);
    moment1.minute(time.minute);
    moment1.second(time.second);
    moment1.millisecond(time.millisecond);
    return moment1;
  }

  public gameStartTime(now: moment.Moment) {
    return GameTimes.time(now, this.gameStart);
  }

  public schoolStartTime(now: moment.Moment) {
    return GameTimes.time(now, this.schoolStart);
  }

  public bellRingTime(now: moment.Moment) {
    return GameTimes.time(now, this.bellRing);
  }


}

/**
 * Game state is all the game relative to today.
 */
export class GameState {
  public readonly active: boolean;
  public readonly nearTheEnd: boolean;
  public readonly upcoming: boolean;
  public readonly past: boolean;
  public readonly notPlaying: boolean;
  public readonly gameStartTime: moment.Moment;
  public readonly schoolStartTime: moment.Moment;
  public readonly bellRingTime: moment.Moment;

  public constructor(times: GameTimes, now: moment.Moment, notPlayingToday: boolean) {
    if (notPlayingToday) {
      this.notPlaying = notPlayingToday;
    } else {
      this.schoolStartTime = times.schoolStartTime(now);
      this.gameStartTime = times.gameStartTime(now);
      this.bellRingTime = times.bellRingTime(now);
      if (now.isSameOrBefore(this.gameStartTime)) {
        this.upcoming = true;
      } else if (now.isBetween(this.gameStartTime, this.schoolStartTime)) {
        this.active = true;
        if (now.isBetween(this.bellRingTime, this.schoolStartTime)) {
          this.nearTheEnd = true;
        }
      } else if(now.isSameOrAfter(this.schoolStartTime)){
        this.past = true;
      }else{
        console.error(`${now.toISOString(true)} not related to times ${this.gameStartTime.toISOString(true)} - ${this.schoolStartTime.toISOString(true)}` );
      }
    }
  }
}
