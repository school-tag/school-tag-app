import {Pipe} from "@angular/core";
import {GameTimeService} from "./game-time.service";
import * as moment from "moment";


@Pipe({name: 'gameWeek'})
export class GameWeekPipe{
  constructor(private gameTimeService:GameTimeService){

  }

  transform(dayString: string|moment.Moment): string {
    if(!(typeof(dayString) === 'string') ){
      dayString = this.gameTimeService.day(dayString)
    }

    return this.gameTimeService.weekDisplay(dayString);
  }
}
