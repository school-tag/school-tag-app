import {Component, Input, OnInit} from "@angular/core";
import {RuleIconService} from "./rule-icon.service";
import {Rule} from "./rule.entity";
import {RuleService} from "./rule.service";
import {DomSanitizer, SafeStyle} from "@angular/platform-browser";

const iconVerticalAlignStyle = '.mat-icon{ vertical-align: text-bottom; }';
const sidePadding = "5px";
//right padding provides cushion with whatever follows, which is typically text.
const iconRightPadding = `.mat-icon{ padding-right: ${sidePadding}`;
//left padding may be controversial since it will not align with others, like test
const iconLeftPadding = `.mat-icon{ padding-left: ${sidePadding}`;
const iconGold = `.mat-icon{ color: gold`;
const blackTextShadow = '-1px -1px 0 #000, 1px -1px 0 #000,-1px 1px 0 #000,1px 1px 0 #000;';
//see RuleIconComponent.coinsRuleId for matching class name
const outline = `.mat-icon{ text-shadow: ${blackTextShadow}`;
const iconStyles = [iconVerticalAlignStyle, iconRightPadding, iconLeftPadding];
const coinIconStyles = iconStyles.concat([iconGold,outline]);

@Component({
  selector: 'points-reward-icon',
  template: '<mat-icon color="primary">{{icon}}</mat-icon>',
  styles: iconStyles,
})
export class PointsRewardIconComponent {
  public icon = RuleIconService.iconForPoints;
  public static readonly iconStyles = iconStyles;
}


@Component({
  selector: 'coins-reward-icon',
  template: '<mat-icon id="coinIcon">{{icon}}</mat-icon>',
  styles: coinIconStyles,
})
export class CoinsRewardIconComponent {
  /**sometimes coins fits in like a rule, so fake a rule id and use this to handle the exception to the rule*/
  public static readonly coinsRuleId = 'coins';
  public icon = RuleIconService.iconForCoins;
}

@Component({
  selector: 'distance-reward-icon',
  template: '<mat-icon color="primary">{{icon}}</mat-icon>',
  styles: iconStyles,
})
export class DistanceRewardIconComponent {
  public icon = RuleIconService.iconForDistance;
}

@Component({
  selector: 'car-reward-icon',
  template: '<mat-icon color="primary">{{icon}}</mat-icon>',
  styles: iconStyles,
})

export class CarRewardIconComponent {
  public icon = 'directions_car';
}


