/**
 * A simple view of the rewards given for some time period, unspecified.
 */
export class
Reward {
  public ruleId: string;
  public points: number;
  public coins: number;
  public metersToSchool: number;
  /**Rule ids indicating the number of times a rule is achieved in the time period.  An object with the rule
   * key
   * */
  public pointsForRules;
}

