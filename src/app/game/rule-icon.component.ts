import {Component, Input, OnInit} from "@angular/core";
import {Rule} from "./rule.entity";
import {RuleService} from "./rule.service";
import {RuleIconService} from "./rule-icon.service";
import {DomSanitizer} from "@angular/platform-browser";
import {CoinsRewardIconComponent, PointsRewardIconComponent,DistanceRewardIconComponent} from "./reward.component";

@Component({
  selector: 'rule-icon',
  templateUrl: 'rule-icon.component.html',
  styles: PointsRewardIconComponent.iconStyles,
})
export class RuleIconComponent implements OnInit {


  /**
   * @param {RuleService} ruleService
   */
  constructor(private ruleService: RuleService) {
  }

  private readonly primaryMaterialColor = 'primary';
  public iconName: string;

  /**
   * Coins aren't a rule, but fit with rules sometimes. If true, show the coin rule instead.
   * @type {boolean}
   */
  public showCoinIcon = false;

  @Input()
  public rule: Rule;

  @Input()
  public ruleId: string;

  @Input()
  public iconColor: string;

  @Input()
  public materialColor: string;

  @Input()
  public defaultIconName: string;

  @Input()
  public showTitle = false;

  ngOnInit(): void {
    //coins are included as a rule sometimes...so handle it specially here.
    if (this.ruleId === CoinsRewardIconComponent.coinsRuleId) {
      this.showCoinIcon = true;
    }
    else if (!this.rule) {
      this.ruleService.ruleById(this.ruleId).subscribe((rule) => {
        this.rule = rule;
        this.iconName = rule.icon;
        if (rule.iconColor) {
          //only assign if not already provided
          if (!this.iconColor) {
            this.iconColor = rule.iconColor;
          }
        } else {
          //only assign if not already provided
          if (!this.materialColor) {
            this.materialColor = this.primaryMaterialColor;
          }
        }
      }, (error)=>{
        this.iconName = this.defaultIconName;
        this.materialColor = this.primaryMaterialColor;
      });
    }
  }
}
