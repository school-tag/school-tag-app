import {Injectable} from "@angular/core";

@Injectable()
export class RuleIconService {

  public static readonly iconForCoins = "monetization_on";
  public static readonly iconForPoints = "poll";
  public static readonly iconForDistance = "add_location";
  private readonly ruleIconMap = {
    everyScanCounts: "done_all",
    morningArrivalEarly: "alarm_on",
    morningArrivalOnTime: "alarm",
    morningArrivalLate: "alarm_off",
    arrivedByBike: "directions_bike",
    arrivedByBus: "directions_bus",
    arrivedByFoot: "directions_walk",
    scannedWalkStation: "directions_walk",
  };

  private readonly defaultIcon = "bug_report";

  constructor() {

  }

  /**
   * given a ruleId, this will produce the associated icon.
   *
   * @param ruleId
   * @returns {string}
   */
  public iconForRule(ruleId) {
    let icon = this.ruleIconMap[ruleId];
    if (!icon) {
      icon = this.defaultIcon;
    }
    return icon;
  }
}
