/**
 * Describes the rule in a way the players and observers can understand.
 */
export class Rule {

  public readonly id: string;
  /**Display as a header*/
  public readonly title: string;
  /**Short description of the rule. */
  public readonly description: string;
  /**The material icon that represents the activity. */
  public readonly icon: string;
  /**The color that best represents the sentiment of the icon. */
  public readonly iconColor: string;
  /**Explains why the rule exists and the reason for the reward incentives. */
  public readonly why: string;
  /**Explanation about how to be rewared for the rule.*/
  public readonly how: string;
  /**Explains some limitations for receiving the reward.*/
  public readonly restrictions: string;
  /**Explains what can be done to improve or words of encouragement for ideal behavior. */
  public readonly encouragement: string;
  /**The number of points rewarded when this rule is achieved. undefined indicates 0*/
  public readonly points: number;
  /**The number of coins rewarded when this rule is achieved. undefined indicates 0*/
  public readonly coins: number;


  /**The rule data in the format provided by the database.*/
  public constructor(ruleData:any){
    const event = ruleData.event;
    this.id=event.type;
    const params = event.params;
    if(params){
      //params must match exactly
      Object.keys(params).forEach((key)=>{
        this[key]=params[key];
      })

    }
  }

}
