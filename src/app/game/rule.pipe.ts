import {Pipe, PipeTransform} from "@angular/core";
import {RuleService} from "./rule.service";
import {Rule} from "./rule.entity";
import {Observable} from "rxjs/Observable";

/**
 * Provides a rule given the ruleId.
 * {{everyScanCounts | rule as rule}}
 * {{rule.title}}
 */
@Pipe({name: 'rule'})
export class RulePipe implements PipeTransform {

  constructor(private ruleService: RuleService) {
  }

  /**
   *
   * @param {string} ruleId
   * @return {Observable<Rule>} the rule matching the rule id
   */
  transform(ruleId: string): Observable<Rule> {
    return this.ruleService.ruleById(ruleId);
  }

}
