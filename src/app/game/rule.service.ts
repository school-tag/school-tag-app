import {Injectable} from "@angular/core";
import {AngularFireDatabase, AngularFireList} from "angularfire2/database";
import {Observable} from "rxjs/Observable";
import {Rule} from "./rule.entity";
import {BehaviorSubject} from "rxjs/BehaviorSubject";

/**
 * Provides the rules that make up the core of the game.  The rules don't change often so this keeps
 * a local copy of the rules around that will be updated if the rules change, but efficient to call
 * many times.
 *
 * The database has the rules in json-rules-engine format which doesn't make good sense to developers here,
 * so the rules is split into
 */
@Injectable()
export class RuleService {
  private rulesByKey: BehaviorSubject<{string,rule}>;

  constructor(private db: AngularFireDatabase) {
    this.rulesByKey = new BehaviorSubject(null);
    db.object('rules').valueChanges().subscribe(this.rulesByKey);
  }

  public ruleById(ruleId: string): Observable<Rule> {
    return Observable.create((observer) => {
      this.rulesByKey.subscribe((rules) => {
        if(rules){
          const rule = rules[ruleId];
          if (rule) {
            observer.next(new Rule(rule));
          } else {
            observer.error(`${ruleId} is not found so something fundamental is wrong`);
          }
        }else{
          observer.error("no rules found");
        }
      }, (error) =>{
        observer.error(error);
      })
    })
  }

  /**
   * Retrieves all of the rules in the order rules are stored.
   */
  public rules(): Observable<Rule[]> {
    return Observable.create((observer) => {
      this.rulesByKey.subscribe((rulesData) => {
        const rules = [];
        observer.next(rules);
        if(rulesData){
          Object.keys(rulesData).forEach((key)=>{
            const rule = new Rule(rulesData[key]);
            rules.push(rule);
          })
        }
      }, (error) =>{
        observer.error(error);
      })
    })
  }

}
