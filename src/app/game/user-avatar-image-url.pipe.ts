import {Pipe, PipeTransform} from "@angular/core";
import {AvatarService} from "./avatar.service";
import {ImageUrl} from "../image/image-url.model";
import {User} from "../users/user.entity";

/**
 * Provides the image url related to the given user for visual identity.
 */
@Pipe({name: 'userAvatarImageUrl'})
export class UserAvatarImageUrlPipe implements PipeTransform {
  constructor(public avatarService: AvatarService) {
  }

  transform(user:User): ImageUrl {
    return  this.avatarService.userAvatarImageUrl(user);
  }
}
