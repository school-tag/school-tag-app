import {Injectable} from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';
import {GameTimeService} from "./game-time.service";
import {Reward} from "./reward.model";
import {Observable} from "rxjs/Observable";
import {Scan} from "../scan/scan.entity";
import * as moment from "moment";

/**
 * Points, coins, badges rewarded to users for their achievements.
 * Provides summaries by day and week.
 *
 */
@Injectable()
export class UserRewardsService {

  constructor(private db: AngularFireDatabase,
              private gameTimeService: GameTimeService) {
  }

  /**
   * the db reference to the daily totals
   * @param userId
   * @param year
   * @param week
   */
   private static weekPath(userId: string, year: number, week: number):string{
    return `/rewards/users/${userId}/totals/weeklies/years/${year}/weeks/${week}`;
  }
   private static daysPath(userId: string, year: number, week: number, dayOfWeek?:number){
    let path = `/rewards/users/${userId}/totals/dailies/years/${year}/weeks/${week}/days`;
    if(dayOfWeek){
      path = `${path}/${dayOfWeek}`;
    }
    return path;
  }

  /**
   *
   * @param userId the rewards for this user
   * @param moment the day of interest or undefined for now
   * @return the rewards for the day represented by the moment
   */
  public totalsForDay(userId: string, moment?:moment.Moment):Observable<Reward>{
    if(!moment){
      moment = this.gameTimeService.now();
    }
    const path = UserRewardsService.daysPath(userId,
      this.gameTimeService.year(moment),
      this.gameTimeService.week(moment),
      this.gameTimeService.dayOfWeek(moment));
    return this.db.object(path).valueChanges();
  }


  /** Summaries for each day a reward was given for the provided week.
   *
   * @param {string} userId
   * @param {number} year
   * @param {number} week
   * @returns {Observable<Reward[]>}
   */
  public dailyTotalsForWeek(userId: string, year: number, week: number):Observable<Reward> {
    let path = UserRewardsService.daysPath(userId,year,week);
    return this.db.object(path).valueChanges();
  }

  /**
   * Provides the totals for a user for the week.
   *
   * @param {string} userId
   * @param {number} year
   * @param {number} week
   * @returns {Observable<Reward>}
   */
  public weekTotals(userId: string, year: number, week: number):Observable<Reward> {
    let path = UserRewardsService.weekPath(userId,year,week);
    return this.db.object(path).valueChanges();
  }

  /**
   * @param {string} userId
   * @param {number} year
   * @returns {Observable<Reward>}
   */
  public yearTotals(userId: string, year: number):Observable<Reward> {
    let path = `/rewards/users/${userId}/totals/yearlies/years/${year}`;
    return  this.db.object(path).valueChanges();
  }

  /**
   * @param {string} userId
   * @returns {Observable<Reward>} totals for the current year
   */
  public thisYearTotals(userId: string):Observable<Reward> {
    return this.yearTotals(userId, this.gameTimeService.thisYear());
  }

  public thisWeekTotals(userId: string):Observable<Reward>{
    return this.weekTotals(userId,this.gameTimeService.thisYear(),this.gameTimeService.thisWeek());
  }

  /** Provides the details of a single day for a user, returning
   * the points given for the rule achieved, related to each scan.
   * [{scanId}/rules/{ruleId}/[points:number]
   *
   * @param {string} userId
   * @param {number} year
   * @param {number} week
   * @param {number} dayOfWeek
   * @returns {Observable<any>}
   */
  public scanRewardsForDay(userId: string, year: number, week: number, dayOfWeek: number) {
    const path = this.scansForDayPath(userId, year, week, dayOfWeek);
    return this.db.object(path).valueChanges();
  }

  private scansForDatePath(userId: string, date: moment.Moment): string {
    let year = this.gameTimeService.year(date);
    let week = this.gameTimeService.week(date);
    let dayOfWeek = this.gameTimeService.dayOfWeek(date);
    return this.scansForDayPath(userId, year, week, dayOfWeek);

  }

  private scansForDayPath(userId: string, year: number, week: number, dayOfWeek: number) {
    return `/rewards/users/${userId}/scans/years/${year}/weeks/${week}/days/${dayOfWeek}`;
  }

  /** Provides the rewards given for a single scan:
   * {
   *    ruleId:{
   *      points: 1,
   *      coins: 1,
   *    }
   * }
   *
   * @param {string} userId provided since the scan has only the tag id
   * @param {Scan} scan providing the timestamp and scan id
   */
  public rewardsForScan(userId: string, scan: Scan):Observable<{string,Reward}> {
    const moment = this.gameTimeService.parseTimestamp(scan.timestamp);
    const path = `${this.scansForDatePath(userId, moment)}/${scan.id}/rules`;
    return this.db.object(path).valueChanges();
  }
}
