/**
 * Manages standard images to take advantage of resizing and auto formatting.
 *
 * Currently uses Imgix. https://dashboard.imgix.com/
 */
export class ImageUrl {
  private _url: string;

  constructor(urlWithPathNoQuery: string) {
    this._url = `${urlWithPathNoQuery}?auto=format,enhance`;
  }

  static from(urlPathNoQueryParams: string) {
    return new ImageUrl(urlPathNoQueryParams);
  }

  /**
   *
   * @param {number} pixels
   * @returns {ImageUrl}
   */
  width(pixels: number) {
    let key = '&w=';
    if(pixels  && this._url.indexOf(key)<0){
      this._url = `${this._url}${key}${pixels}`;
    }
    return this;
  }

  /**
   *
   * @param {number} pixels
   * @returns {ImageUrl}
   */
  height(pixels: number) {
    let key = '&h=';
    if(pixels && this._url.indexOf(key)<0){
      this._url = `${this._url}${key}${pixels}`;
    }
    return this;
  }

  /**
   * @returns {string}
   */
  url(){
    return this._url;
  }

  toString() {
    return this._url;
  }
}
