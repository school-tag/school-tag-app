import {Injectable} from "@angular/core";
import {ImageUrl} from "./image-url.model";

/**
 * Manages finding and serving images managed by the application.
 *
 * Google Cloud Storage Buckets, via Firebase Cloud Storage, provides access to list/read/write images.
 *
 * Imgix serves the images up in a web friendly fashion (resizing, caching, etc).
 *
 *
 */
@Injectable()
export class ImageService {

  //FIXME: this should be provided
  /** the protocol, domain and path to the root of where images are found. Subfolders can exist as appropriate.
   * no query params*/
  private hostUrl: string;

  constructor(hostUrl: string) {
    this.hostUrl = hostUrl;
  }

  /**
   *
   * @param {string} path with leading slash preferred
   * @returns {string}
   */
  public withPath(path: string) {
    if (path && !path.startsWith('/')) {
      path = `/${path}`;
    }
    return ImageUrl.from(`${this.hostUrl}${path}`);
  }

}
