import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {DataSource} from '@angular/cdk/collections';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {Router} from "@angular/router";
import {MatSnackBar} from "@angular/material";
import {AppRoutes} from "../app.routes";
import {PlayerRank} from "./player-rank.entity";
import {PlayerRankService} from "./player-rank.service";

@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.css']
})
export class LeaderboardComponent implements OnInit, OnDestroy {
  public displayedColumns: string[];
  public dataSource: PlayerRanksDatasource;

  @Input()
  public ruleId:string;

  constructor(private router: Router,
              private snackBar: MatSnackBar,
              private playerRankService: PlayerRankService) {

  }

  ngOnInit(): void {
    this.displayedColumns = ['rank', 'nickname', 'points'];

    let ranks:Observable<PlayerRank[]>;

    if(this.ruleId){
      ranks = this.playerRankService.gameTotalPlayerRanksForRule(this.ruleId);
    }else{
      ranks = this.playerRankService.gameTotalPlayerRanks();
    }
    this.dataSource = new PlayerRanksDatasource(ranks);
  }

  ngOnDestroy(): void {
    this.dataSource.disconnect();
  }

  public viewUser(userId: string) {
    AppRoutes.navigate(AppRoutes.userDayPath(userId), this.router, this.snackBar);
  }
}

/**
 * Data source to provide what data should be rendered in the table. The observable provided
 * in connect should emit exactly the data that should be rendered by the table. If the data is
 * altered, the observable should emit that new set of data on the stream.
 */
export class PlayerRanksDatasource extends DataSource<PlayerRank> {
  constructor(private ranks:Observable<PlayerRank[]>) {
    super();
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<PlayerRank[]> {
    return this.ranks;
  }

  disconnect() {
  }
}


