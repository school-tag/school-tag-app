import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {MomentModule} from "angular2-moment";
import {LeaderboardComponent} from "./leaderboard.component";
import {
  MatCardModule, MatIconModule, MatSnackBarModule, MatTableModule, MatTabsModule,
  MatToolbarModule
} from "@angular/material";
import {AvatarImageUrlPipe} from "../game/avatar-image-url.pipe";
import {LeaderboardsComponent} from "./leaderboards.component";
import {RulesModule} from "../rules/rules.module";
import {PlayerRankPipe} from './player-rank.pipe';


@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatIconModule,
    MatSnackBarModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MomentModule,
    RulesModule,
  ],
  declarations:[
    LeaderboardComponent,
    LeaderboardsComponent,
    AvatarImageUrlPipe,
    PlayerRankPipe,
  ],
  exports: [
    AvatarImageUrlPipe,
    LeaderboardComponent,
    PlayerRankPipe,
  ]
})
export class LeaderboardModule{
}
