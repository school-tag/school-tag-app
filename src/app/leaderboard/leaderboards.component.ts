import {Component, OnDestroy, OnInit} from "@angular/core";


@Component({
  selector: 'app-leaderboards',
  templateUrl: './leaderboards.component.html',
  styleUrls: ['./leaderboards.component.css']
})
export class LeaderboardsComponent implements OnInit, OnDestroy {
  constructor(){

  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }

}
