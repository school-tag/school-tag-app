/**
 * used by the leaderboard to order the players in the competition.
 *
 */
export interface PlayerRank {
  rank: number;
  nickname: string;
  points: number;
  userId: string;
  avatar: string;
}
