import {Pipe} from "@angular/core";
import {PlayerRankService} from './player-rank.service';
import {Observable} from 'rxjs';
import {PlayerRank} from './player-rank.entity';


@Pipe({name: 'playerRank'})
export class PlayerRankPipe{
  constructor(private playerRankService:PlayerRankService){

  }

  transform(userId: string): Observable<PlayerRank> {
    return this.playerRankService.gameTotalPlayerRankForUser(userId);
  }
}
