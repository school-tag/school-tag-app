import {AngularFireDatabase, QueryFn} from "angularfire2/database";
import {Observable} from "rxjs/Observable";
import {PlayerRank} from "./player-rank.entity";
import {Injectable} from "@angular/core";

const ranksPath = `ranks`;
const gameTotalRanksPath = `${ranksPath}/game`;
const ranksRulesPath = `${ranksPath}/rules`;

@Injectable()
export class PlayerRankService {
  constructor(private db: AngularFireDatabase) {
  }

  /**
   *
   * @return {Observable<PlayerRank[]>} stream of player ranks in order or best to worst rank.
   */
  public gameTotalPlayerRanks(): Observable<PlayerRank[]> {
    return <Observable<PlayerRank[]>> this.db.list((gameTotalRanksPath)).valueChanges();
  }


  /**
   *
   * @param {string} ruleId
   * @return {Observable<PlayerRank[]>}
   */
  public gameTotalPlayerRanksForRule(ruleId:string){
    return <Observable<PlayerRank[]>> this.db.list(`${ranksRulesPath}/${ruleId}`).valueChanges();
  }

  /**Single rank for the given user.
   *
   * @param {string} userId
   * @return {Observable<PlayerRank>} matching the user id given.
   */
  gameTotalPlayerRankForUser(userId: string) {
    return <Observable<PlayerRank>> Observable.create((observer) => {
      this.gameTotalPlayerRanks().subscribe((playerRanks) => {
        const match = playerRanks.find((playerRank) => {
          return playerRank && playerRank.userId == userId;
        });
        if (match) {
          observer.next(match);
        } else {
          observer.error(`${userId} not found in playerRanks`);
        }
      });
    });
  }
}
