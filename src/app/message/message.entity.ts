/**
 * Used for streaming information as it arrives, this provides text and optional associated
 * information about the message.
 */
import { User } from '../users/user.entity';
import {Reward} from "../game/reward.model";

export interface Message {
  /** The main content human readable intended for the end user */
  text: string;
  /** ISO 8601 formatted string of when the scan happened as reported by the scanner. */
  timestamp: string;
  /** Optionally describes the context about who the text describes. */
  aboutUser: User;

  /**
   * Icon that represents the message intent or source.
   */
  iconUrl: string;

  /**
   * Custom payload optionally provided to explain the details included in the message.
   */
  aboutPayload: Reward;
}
