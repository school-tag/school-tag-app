import {Injectable} from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';
import * as moment from 'moment';
import {GameTimeService} from "../game/game-time.service";
import {Observable} from "rxjs/Observable";

@Injectable()
export class MessageService {

  constructor(private db: AngularFireDatabase,
              private gameTimeService: GameTimeService) {

  }


  /**
   * Shows the most recent messages received.  Uses primary key, not timestamp so it may have some order
   * introduced by the system processing and not reflect absolute reality.
   *
   * @param limit of messages desired
   */
  public mostRecentMessages(limit:number){

    let queryFn = (ref) => {
      return ref.orderByKey().limitToLast(limit);
    };
    return this.reversed(this.messageList(queryFn));
  }
  /**
   *
   * @param {string} dateString in ISO date format YYYY-MM-DD
   * @returns {Observable<Message[]>} ordered with most recent first
   */
  public messagesForDay(dateString?: string) {
    const date = (dateString)?moment.parseZone(dateString): this.gameTimeService.now();

    let queryFn = (ref) => {
      let start = date.startOf('day').format();
      let end = date.endOf('day').format();
      return ref.orderByChild('timestamp').startAt(start).endAt(end);
    };
    return this.reversed(this.messageList(queryFn));
  }

  /**
   * Runs the query given.
   * @param queryFn
   */
  private messageList(queryFn) {
    return this.db.list('messages', queryFn);
  }

  /**
   * Reverses the order of the list returned since messages are most recent first, but queries are ascending timestamp.
   * @param angularFireList from the database
   */
  private reversed(angularFireList) {
    return Observable.create((observer) => {
      angularFireList.valueChanges().subscribe((messages) => {
        observer.next(messages.reverse());
      });
    });
  }

  /**
   *
   * @param {string} dateString
   * @param {(ref) => any} queryFn
   * @returns {Observable<Message[]>}
   */
  private messagesForDate(dateString: string, queryFn: (ref) => any) {
    let path = 'messages';
    return
  }


}
