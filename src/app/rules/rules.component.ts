import {Component, OnInit} from "@angular/core";
import {RuleService} from "../game/rule.service";
import {Observable} from "rxjs/Observable";
import {Rule} from "../game/rule.entity";
import {AppRoutes} from "../app.routes";



@Component({
  selector: 'app-rules',
  templateUrl: './rules.component.html',
  styleUrls: ['./rules.component.css']
})
export class RulesComponent implements OnInit {

  public readonly prizesSelector = AppRoutes.prizesPath;

  private rules: Observable<Rule[]>;


  constructor(private ruleService: RuleService) {
  }

  ngOnInit(): void {
    this.rules = this.ruleService.rules();
  }

}
