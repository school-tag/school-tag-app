import {NgModule} from "@angular/core";
import {RuleService} from "../game/rule.service";
import {RulesComponent} from "./rules.component";
import {RulePipe} from "../game/rule.pipe";
import {CommonModule} from "@angular/common";
import {MomentModule} from "angular2-moment";
import {MaterialModule} from "../material.module";
import {
  CarRewardIconComponent, CoinsRewardIconComponent, PointsRewardIconComponent, DistanceRewardIconComponent
} from "../game/reward.component";
import {RuleIconComponent} from "../game/rule-icon.component";
import {AppRoutingModule} from "../app-routing.module";


@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    MomentModule,
    AppRoutingModule
  ],
  declarations:[
    CoinsRewardIconComponent,
    PointsRewardIconComponent,
    DistanceRewardIconComponent,
    CarRewardIconComponent,
    RuleIconComponent,
    RulesComponent,
    RulePipe,

  ],
  providers:[
    RuleService,
  ],
  exports: [
    CoinsRewardIconComponent,
    PointsRewardIconComponent,
    DistanceRewardIconComponent,
    CarRewardIconComponent,
    RuleIconComponent,
    RulePipe,
  ]
})
export class RulesModule{
}
