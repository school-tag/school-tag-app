import {Component, Inject, OnInit} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef, MatSnackBar} from "@angular/material";
import {Scan} from "./scan.entity";
import {User} from "../users/user.entity";
import {TagCategory} from "../tag/tag-category.entity";
import {Observable} from "rxjs/Observable";
import {UserRewardsService} from "../game/user-rewards.service";
import {RewardDetailsDialogComponent} from "../user/reward-details-dialog.component";
import {UserService} from '../users/user.service';
import {TagService} from '../tag/tag.service';
import {AppRoutes} from '../app.routes';
import {Router} from '@angular/router';

/**
 * Shows the details of a single scan in a dialog.
 */
@Component({
  selector: 'app-scan-dialog',
  templateUrl: './scan-dialog.component.html',
  styleUrls: ['./scan-dialog.component.css']
})
export class ScanDialogComponent implements OnInit {

  public scan: Scan;
  public tagCategory: TagCategory;
  public rewardsForScan: Observable<{ string, Reward }>;
  /** The tag owner that got scanned. */
  public scannedUser: Observable<User>;
  /** The authenticated user that scanned the tag, if any. */
  public scanningUser: Observable<User>;

  public readonly avatarPixels:20;
  constructor(public dialogRef: MatDialogRef<ScanDialogComponent>,
              @Inject(MAT_DIALOG_DATA) private data: any,
              private userRewardsService: UserRewardsService,
              private tagService: TagService,
              private userService: UserService,
              private router: Router,
              private snackBar: MatSnackBar,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.scan = this.data.scan;
    if (this.scan.authUid) {
      this.scanningUser = this.userService.userByAuthId(this.scan.authUid);
    }
    this.tagService.tagById(this.scan.tagId).subscribe((tag) => {
      this.rewardsForScan = this.userRewardsService.rewardsForScan(tag.userId, this.scan);
      this.scannedUser = this.userService.userById(tag.userId);
    })
  }

  public viewRewardDetails(ruleId, valueOfReward) {
    const config: MatDialogConfig = {
      data: {
        ruleId: ruleId,
        value: valueOfReward,
        period: "this scan",
      },
      width: '350px',
    };
    this.dialog.open(RewardDetailsDialogComponent, config);
  }

  /**View the user scoreboard.
   *
   * @param userId
   */
  public viewUser(userId: string) {
    this.dialog.closeAll();
    AppRoutes.navigate(AppRoutes.userDayPath(userId), this.router, this.snackBar);
  }

  /** Shows a snackbar exposing the id, mostly for admins. */
  viewStation(stationId: string) {
    this.snackBar.open(stationId,'OK', {
      duration: 3000
    });
  }
}
