import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ScanComponent} from './scan.component';
import {MaterialModule} from "../material.module";
import {DaysModule} from "../days/days.module";
import {AppModule} from "../app.module";
import {RouterTestingModule} from "@angular/router/testing";
import {AngularFireAuth} from "angularfire2/auth";
import {AngularFireModule, FirebaseApp, FirebaseAppProvider} from "angularfire2";
import {environment} from "../../environments/environment";
import {AngularFireDatabase} from "angularfire2/database";
import {GameTimeService} from "../game/game-time.service";
import {UserService} from "../users/user.service";
import {ScanService} from "./scan.service";
import {TagService} from "../tag/tag.service";
import {TagCategoryService} from "../tag/tag-category.service";
import {BrowserAnimationsModule, NoopAnimationsModule} from "@angular/platform-browser/animations";
import {By} from "@angular/platform-browser";
import {ActivatedRoute} from "@angular/router";
import {ActivatedRouteStub} from "../../testing/router-stubs";

describe('ScanComponent', () => {
  let component: ScanComponent;
  let fixture: ComponentFixture<ScanComponent>;
  const gameTimeService = new GameTimeService("-02:00");
  const userService = {};
  const scanService = {};
  const tagService = {};
  const tagCategoryService = {};
  const angularFireAuth = {};
  const angularFireDatabase = {};
  const activatedRoute = new ActivatedRouteStub();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ScanComponent],
      imports: [RouterTestingModule, MaterialModule, NoopAnimationsModule],
      providers: [AngularFireDatabase,
        {provide: ActivatedRoute, useValue: activatedRoute},
        {provide: AngularFireAuth, useValue: angularFireAuth},
        {provide: UserService, useValue: userService},
        {provide: ScanService, useValue: scanService},
        {provide: TagService, useValue: tagService},
        {provide: TagCategoryService, useValue: tagCategoryService},
        {provide: AngularFireAuth, useValue: angularFireAuth},
        {provide: AngularFireDatabase, useValue: angularFireDatabase},
        {provide: GameTimeService, useValue: gameTimeService}
      ]
    })
      .compileComponents().then(() => {
      fixture = TestBed.createComponent(ScanComponent);
      component = fixture.componentInstance;
    })
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show spinner by default', () => {
    const spinner = fixture.debugElement.query(By.css('.container'));
    expect(spinner.name).toBe('mat-spinner');
    expect(spinner.styles['visibility']).toBeUndefined();
  });
  describe('tagIdQueryParameters', () => {
    it('should forward to school day and post a message if no tagId is given ', () => {
      activatedRoute.testQueryParamMap = {}
      fixture.detectChanges();
      fixture.whenRenderingDone().then(() => {
        expect(component.scan).toBeDefined();
        expect(component.scan).not.toBeNull();
        expect(component.scan.tagId).toBeUndefined();
      })
    });
    it('should find the tag id and counter provided ', () => {
      activatedRoute.testQueryParamMap = {'m': 'abc-123x00F'}
      const fIs15InHex = 15;
      fixture.detectChanges();
      fixture.whenRenderingDone().then(() => {
        expect(component.scan.tagId).toEqual('abc-123');
        expect(component.scan.counter).toEqual(fIs15InHex);
      })
    });
  });
});
