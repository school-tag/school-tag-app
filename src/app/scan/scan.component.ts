import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AngularFireAuth} from "angularfire2/auth";
import {Scan} from "./scan.entity";
import {ScanRequirement, ScanService} from "./scan.service";
import {TagService} from "../tag/tag.service";
import {AppRoutes} from "../app.routes";
import {Observable} from "rxjs/Observable";
import {UserService} from "../users/user.service";
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/forkJoin';
import {MatSnackBar} from "@angular/material";

/**
 * Gathers required information before sending a scan for rewards:
 * 1. Authenticated user
 *   a. Require user to sign in
 * 2. TagId to be rewarded
 *   a. Fails otherwise since it must be in the url.  Scan again
 * 3. The player who owns the tag id
 *   a. Tag has user id already assigned - happy case
 *   b. Tag has no user so offer to find existing
 *   c. Tag has no user so create a new one with the scan
 */
@Component({
  selector: 'app-scan',
  templateUrl: './scan.component.html',
  styleUrls: ['./scan.component.css']
})
export class ScanComponent implements OnInit {

  /**The tag id is produced by the smart tag, written by School Tag minions.*/
  private readonly tagIdQueryParamKey = 'm';
  public scan: Scan;

  private readonly snackBarOptions = {duration: 5000};
  //the user playing the game, as indicated by the tag
  //if no user represents the tag then an opportunity to assign the tag to an existing user is given
  private playerId: Observable<string>;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private snackBar: MatSnackBar,
              private firebaseAuth: AngularFireAuth,
              private scanService: ScanService,
              private tagService: TagService,
              private userService: UserService) {
  }

  ngOnInit() {
    //using snapshot won't listen for updates, but scans don't require it.
    const queryParamMap = this.route.snapshot.queryParamMap;
    const tagIdCounterParam = queryParamMap.get(this.tagIdQueryParamKey);
    //station tags have category and station encoded into the path
    const pathParamMap = this.route.snapshot.paramMap;
    const categoryParam = pathParamMap.get(AppRoutes.tagCategoryParamKey);
    const typeParam = pathParamMap.get(AppRoutes.tagTypeParamKey);
    this.handleRequirementsAndSendScan(tagIdCounterParam, typeParam, categoryParam);
  }

  /**
   * Given information collected from the scan parameters, this will forward to requirements
   * or send the scan and forward to the appropriate success page.
   *
   * @param {string} tagIdCounterParam
   * @param {boolean} typeParam
   * @param {string} categoryParam that identifies the station's purpose (walk, bus, car, etc).
   */
  private handleRequirementsAndSendScan(tagIdCounterParam: string, typeParam: string, categoryParam: string) {
//build the scan with all that is known
    this.scan = ScanService.scanFromParam(tagIdCounterParam);
    if (categoryParam) {
      this.scan.tagCategory = categoryParam;
    }
    if(typeParam){
      this.scan.tagType = typeParam;
    }
    this.assignAuthUserToScanIfAuthenticated().subscribe(() => {
      const requirements = ScanService.additionalRequirementsToSend(this.scan);

      if (requirements.length === 0) {
        this.sendScan();
      } else {
        //handle one requirement at a time, the most important first
        const requirement = requirements[0];
        switch (requirement) {
          case ScanRequirement.authentication:
            this.forwardToAuthentication();
            break;
          case ScanRequirement.tagId:
            this.tagIsMissing();
            break;
          default:
            console.assert(false, `You must handle ${requirement}`);
        }
      }
    });
  }

  /**
   * Only assigns the authId to scan if already provided. Useful to report authentication even if not required.
   * @return {Observable<void>}
   */
  private assignAuthUserToScanIfAuthenticated() {
    return Observable.create((observer) => {
      this.firebaseAuth.authState.subscribe((authUser) => {
        if (authUser) {
          this.scan.authUid = authUser.uid;
        }
        observer.next();
      }, (error) => {
        observer.error(error);
      });
    });
  }


  /**
   * sends the scan that is assumed to have all requirements handled.
   */
  private sendScan() {
    this.scanService.scanReceived(this.scan).subscribe(() => {
      if (ScanService.tagIsStation(this.scan)) {
        this.snackBar.open('Scan sent...calculating rewards', '', this.snackBarOptions);
        this.forwardToAuthentication();
      } else {
        this.forwardToTagOwner(this.scan.tagId);
      }
    }, (error) => {
      console.log('problem sending scan', error);
      this.snackBar.open('Oops, problem sending scan.  Please scan again', 'Got it');
    })
  }

  private forwardToDay() {
    AppRoutes.navigate(AppRoutes.daySelector(), this.router, this.snackBar);
  }


  /**
   * Viewing the Player Scoreboard is the typical desire for most scans.
   * Sometimes the scans will increase points.
   * Sometimes the scanner wishes to reward the player for some bonus points.
   *
   * @param {string} tagId
   */
  private forwardToTagOwner(tagId: string) {

    this.tagService.tagById(tagId).subscribe((tag) => {
      if (tag) {
        const userId = tag.userId;
        if (userId) {
          this.scanService.offerToRewardIfPossible(this.scan).subscribe(()=>{
            this.forwardToPlayer(userId);
          });
        } else {
          this.forwardToAuthentication();
        }
      } else {
        if(this.scan.userIdOfTagOwner){
          this.snackBar.open(`Assigning tag to user ${this.scan.userIdOfTagOwner}`,'OK', this.snackBarOptions)
        }else{
          this.snackBar.open('This tag is an orphan:(', '', this.snackBarOptions)
          this.forwardToDay();
        }
      }

    })
  }

  /**
   * Some situations require an authenticated user of the app.
   * 1. Scanning a station, the player is using their own phone.
   */
  private forwardToAuthentication() {
    if (this.scan.authUid) {
      //already authenticated...go to the player page, if it exists
      this.userService.userByAuthId(this.scan.authUid).subscribe((user) => {
        if (user) {
          this.forwardToPlayer(user.id);
        } else {
          //identifiable, but not a player...
          this.notPlaying();
        }
      })
    } else {
      this.snackBar.open('Sign in to earn rewards.', 'Sign In').onAction().subscribe(() => {
        AppRoutes.signIn(this.router, this.snackBar);
      });
    }
  }


  /**
   * Standard messaging and forwarding when determined the viewer has no tag or authentication to identify source
   */
  private notPlaying() {
    this.snackBar.open('Not playing School Tag?  See a minion', '', this.snackBarOptions);
    this.forwardToDay();
  }

  /**Views the player's screen. This is after a scan or if viewing a backpack tag or any non-station.
   *
   * @param {string} playerUserId
   */
  private forwardToPlayer(playerUserId: string) {
    //forward to the participant user of the tag.
    this.router.navigateByUrl(AppRoutes.userDayPath(playerUserId)).catch((error) => {
      console.error("unable to navigate", error);
    });
  }


  /**
   *
   */
  private tagIsMissing() {
    this.snackBar.open('You scanned a rotten tag!', 'Whatever...', this.snackBarOptions)
      .afterDismissed().subscribe(() => {
      this.forwardToDay();
    });
  }
}
