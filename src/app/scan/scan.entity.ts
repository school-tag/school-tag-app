/**
 * A single read of a tag.
 */
export class Scan {

  /**the server provided unique identifier of the scan.
   * The ids are string that are sortable in ascending time of received
   * by the server.
   */
  public id: string;
  /**
   * The UID of the Tag, provided by the tag in the url.
   */
  public tagId: string;

  /**
   * Tags have a counter built into the URL.
   * {tagId}x{counter}.  This is a one up counter every time the tag is scanned.
   * It is useful for debugging (scan made, but internet failed or scan never made).
   */
  public counter: number;

  /** The firebase UID that identifies the authenticated user.
   * This may be the station manager or a participant scanning a tag.
   * */
  public authUid: string;

  /**
   * ISO date/time string in game time, of the moment of the scan.
   */
  public timestamp: string;

  /**The category the tag is reporting via the category query parameter, if any. Examples are walk, bus*/
  public tagCategory: string;

  /**
   * A major role for the tag to differentiate from other tags. Provided here for easy setup of a server tag.
   *
   * 1. station - a tag, typically a sticker, placed in the community that is related to no single person
   * 2. personal - a tag, typically a hangtag, owned by a single person...typically hanging on a students backpack
   */
  public tagType: string;


  /**
   * A way to manually provide an achievement of a category by the person scanning a tag. For example, a minion scans
   * a backback tag for a student getting off the bus would receive category=bus to be rewarded for taking the bus.
   */
  public category: string;

  /**
   * Optional and rarely used, this provided will indicate that the tag being scanned is likely new and should
   * be associated to the identified user.
   */
  public userIdOfTagOwner: string;

  /**
   * the id of the station which scanned the player.
   */
  public station: string;
}
