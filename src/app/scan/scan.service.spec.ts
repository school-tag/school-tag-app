import {TestBed, inject} from '@angular/core/testing';

import {ScanRequirement, ScanService} from './scan.service';
import {AngularFireDatabase} from "angularfire2/database";
import {GameTimeService} from "../game/game-time.service";
import {Scan} from "./scan.entity";
import {fail} from "assert";
import {stationTagType} from "../tag/tag.service";
import {serialize} from "@angular/compiler/src/i18n/serializers/xml_helper";

//FIXME:Get this from a standard place
const gameTimeService = new GameTimeService("-02:00");

describe('ScanService', () => {
  const scanWithTagId = new Scan();
  scanWithTagId.tagId = 'tag-id-provided';

  const scanOfStationWithoutAuthentication = {...scanWithTagId};
  scanOfStationWithoutAuthentication.tagType = stationTagType;

  const scanOfStationWithAuthentication = {...scanOfStationWithoutAuthentication};
  scanOfStationWithAuthentication.authUid = 'auth-id-for-station';

  describe('injectable', () => {
    beforeEach(() => {
      TestBed.configureTestingModule({
        providers: [ScanService,
          {provide: AngularFireDatabase, useValue: {}},
          {provide: GameTimeService, useValue: gameTimeService}
        ]
      });
    });
    it('should be created', inject([ScanService], (service: ScanService) => {
      expect(service).toBeTruthy();
    }));
    describe('scanReceived', () => {
      let assertMissingRequirement = function (scan: Scan,
                                               expectedRequirement: ScanRequirement,
                                               service: ScanService,
                                               done: Function) {
        const observable = service.scanReceived(scan);
        observable.subscribe(() => {
          expect(false).toBe(true);
          done("expecting an error");
        }, (requirements) => {
          expect(requirements).not.toBeNull();
          expect(requirements).toContain(expectedRequirement);
          done();
        });
      };
      it('should reject a scan without a tag id', (done) => {
        inject([ScanService], (service: ScanService) => {
          const scan = new Scan();
          assertMissingRequirement(scan, ScanRequirement.tagId, service, done);
        })();
      });
      it('should reject a station scan without authUid', (done) => {
        inject([ScanService], (service: ScanService) => {
          assertMissingRequirement(scanOfStationWithoutAuthentication, ScanRequirement.authentication, service, done);
        })();
      });
      it('should provide the player associated with the tag id');
      it('should provide the player associated with the authUid for a station');
      it('should assign a timestamp to the scan', (done) => {
        return inject([ScanService,AngularFireDatabase], (service: ScanService,db:AngularFireDatabase) => {
          return service.scanReceived(scanWithTagId).subscribe((scanWithTimestamp)=>{
            return expect(scanWithTimestamp.timestamp).not.toBeNull();
          });
        })();
      });
      it('should assign an id to the scan', () => {

      });
    });
    describe('offerToReward', () => {
      it('should be possible if authenticated and tag id is available', () => {
        return inject([ScanService,AngularFireDatabase], (service: ScanService,db:AngularFireDatabase) => {
          return service.offerToRewardIfPossible(scanOfStationWithAuthentication).subscribe((stored)=>{
            expect(stored).toBeTruthy();
            return service.offerCanBeRewarded([]).subscribe((rewardCanBeOffered)=>{
              return expect(rewardCanBeOffered).toBeTruthy();
            })
          });
        })();
      });
    });
  });
  describe('scanFromParam', () => {
    const tagId = "abc-def"
    const counterHex = "0000F"; //hex
    const counterNumber = 15;
    const tagIdCounterParam = `${tagId}x${counterHex}`;
    it('should return empty scan with empty param', () => {
      const scan = ScanService.scanFromParam(null);
      expect(scan).not.toBeNull();
      expect(scan.tagId).toBeUndefined();
    });
    it('should return scan with tag id only', () => {
      const scan = ScanService.scanFromParam(tagId);
      expect(scan.tagId).toBe(tagId);
      expect(scan.counter).toBeUndefined();
    });
    it('should return scan with tag Id and counter', () => {
      const scan = ScanService.scanFromParam(tagIdCounterParam);
      expect(scan.tagId).toBe(tagId);
      expect(scan.counter).toBe(counterNumber);
    });
  });
  describe('requirements', () => {
    it('should indicate a Tag id is required', () => {
      const scan = new Scan();
      const requirements = ScanService.additionalRequirementsToSend(scan);
      expect(requirements).toContain(ScanRequirement.tagId);
    });
    it('should not indicate a Tag id is required if provided', () => {
      const scan = new Scan();
      scan.tagId = 'tag-id-provided';
      const requirements = ScanService.additionalRequirementsToSend(scan);
      expect(requirements).not.toContain(ScanRequirement.tagId);
    });
    it('should indicate authUid is required for a station', () => {

      const requirements = ScanService.additionalRequirementsToSend(scanOfStationWithoutAuthentication);
      expect(requirements).toContain(ScanRequirement.authentication);

    });
    it('should not indicate authUid is required for a non-station', () => {
      const scan = new Scan();
      const requirements = ScanService.additionalRequirementsToSend(scan);
      expect(requirements).not.toContain(ScanRequirement.authentication);

    });
    it('should not indicate authUid is required for a station when authUid is provided', () => {

      const requirements = ScanService.additionalRequirementsToSend(scanOfStationWithAuthentication);
      expect(requirements).not.toContain(ScanRequirement.authentication);
    });
  });
  describe('tagIsStation', () => {
    it('should return true if station', () => {
     return expect(ScanService.tagIsStation(scanOfStationWithoutAuthentication)).toBeTruthy();
    });
    it('should return false if not a station', () => {
     return expect(ScanService.tagIsStation(scanWithTagId)).toBeFalsy();
    });
  });

});
