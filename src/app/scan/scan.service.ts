import {Injectable} from '@angular/core';
import {Scan} from "./scan.entity";
import {AngularFireDatabase} from "angularfire2/database";
import {GameTimeService} from "../game/game-time.service";
import {AngularFireAuth} from "angularfire2/auth";
import {Observable} from "rxjs/Observable";
import {TagService} from "../tag/tag.service";
import {ErrorObservable} from "rxjs/observable/ErrorObservable";
import {AsyncLocalStorage} from "angular-async-local-storage";
import * as moment from "moment";
import {Tag} from "../tag/tag.entity";
import {UserService} from '../users/user.service';

/** indications of what will be required to send a scan, intended for users to resolve.*/
export enum ScanRequirement {
  /**sign in required*/
  authentication,
  /* tag id is missing */
  tagId,
  /**a bike tag that is orphaned and needs to be partnered with backpack tag*/
  associateBikeTag
}

/**Used to communicate the next scan is to be associated to a user.*/
const associateTagStorageKey = 'associateTag';
/**Used to communicate the user is authenticated and may wish to send a bonus reward.*/
const offerBonusRewardsStorageKey = 'offerBonusRewards';
/**Indicates the user has scanned, but is not authenticated and if they were then they could offer bonus*/
const offerBonusRewardsIfSignedInStorageKey = 'offerBonusRewardsIfSignedIn';

/**persisted in local storage indicating the next scan should associate the tag to the user.*/
class TagAssociation {
  /** When the user id is known, the tag will be associated to the user.*/
  public readonly userId: string;
  /** When the authUid is known, then this will be associated to the user that owns the tag.*/
  public readonly authUid: string;
  public readonly expires: string; //timestamp string
}

/**
 * Sends scans to the cloud to be rewarded.  Ensures the scan has minimum information
 * and reports requirements before sending.
 */
@Injectable()
export class ScanService {


  constructor(private db: AngularFireDatabase,
              private gameTimeService: GameTimeService,
              private localStorage: AsyncLocalStorage,
              private firebaseAuth: AngularFireAuth,
              private userService: UserService,
              private tagService: TagService) {
  }

  /**
   * Called first, this will provide any synchronous pre-checks
   * @param {Scan} scan
   * @return {ScanRequirement[]}
   */
  public static additionalRequirementsToSend(scan: Scan) {
    const requirements = []
    if (!scan.tagId) {
      requirements.push(ScanRequirement.tagId);
    }
    if (ScanService.tagIsStation(scan) && !scan.authUid) {
      requirements.push(ScanRequirement.authentication);
    }
    return requirements;
  }

  /**
   * Most recent scans for the tag given ordered by most recent first.
   *
   * @param {string} tagId used in the scan
   * @param {string} day the scan took place (YYYY-MM-dd)
   * @return {Observable<Scan[]>}
   */
  public scansForTag(tagId: string, day:string): Observable<Scan[]> {
    return <Observable<Scan[]>> Observable.create((observer) => {
      this.db.list('/scans/' + day, (ref) => {
        return ref.orderByChild('tagId').equalTo(tagId).limitToLast(100);
      }).valueChanges().subscribe((scans) => {
        observer.next(scans.sort(ScanService.mostRecentScansSortFunction()));
      });
    })
  }

  /**
   *
   * @param {Scan} scan to be sent, already passing additionalRequirementsToSend
   * @returns {Observable<Scan>} scan with the cloud assigned scan.id
   * @throws {ErrorObservable}
   */
  public scanReceived(scan: Scan): Observable<Scan> {
    if (!scan.timestamp) {
      scan.timestamp = this.gameTimeService.timestampString(this.gameTimeService.now());
    }
    return <Observable<Scan>> Observable.create((observer) => {
      const requirements = ScanService.additionalRequirementsToSend(scan);
      if (requirements.length > 0) {
        observer.error(requirements);
      } else {
        scan.tagId = TagService.formatTagId(scan.tagId);
        this.assignAuthUserToScanIfAuthenticated(scan).subscribe(() => {
          //associate tag to owner if flagged to do so
          this.associationForNextScan().subscribe((tagAssociation) => {
            if (tagAssociation) {
              if (tagAssociation.userId) {
                //communicates to the server that the tag should be associated to the user id
                scan.userIdOfTagOwner = tagAssociation.userId;
              } else if (tagAssociation.authUid) {
                //user is authenticated so the write can just happen
                if (scan.authUid == tagAssociation.authUid) {
                  this.tagService.tagById(scan.tagId).subscribe(tag => {
                    this.userService.assignAuthUidToUser(scan.authUid, tag.userId).subscribe(() => {
                      console.log(`successfully associated ${scan.authUid} to ${tag.userId}`);
                      //TODO:report the success to the user..maybe return an error?
                    }, error => {
                      observer.error(error);
                    })
                  }, error => {
                    observer.error(error);
                  })
                } else {
                  observer.error(`tag association gave authUid of ${tagAssociation.authUid}, but scan gave ${scan.authUid}.`)
                }
              }
            }
            const thenableReference = this.db.list('scans').push(scan);
            thenableReference.then(() => {
              scan.id = thenableReference.key;
              observer.next(scan);
              return scan;
            }).then((scan) => {
              return thenableReference.child('id').set(scan.id);
            }, (error) => {
              observer.error(error);
            });
          });
        });
      }
    });
  }


  /**
   * For scans that have sufficient information, the user can be offered to reward the player with another bonus scan.
   * This stores a flag in the local storage and should be used with
   * @param {Scan} scan
   */
  public offerToRewardIfPossible(scan: Scan): Observable<boolean> {
    if (scan && scan.tagId) {
      let storeKey;
      if (scan.authUid) {
        storeKey = offerBonusRewardsStorageKey;
      } else {
        storeKey = offerBonusRewardsIfSignedInStorageKey;
      }
      return this.localStorage.setItem(storeKey, scan.tagId);
    } else {
      this.offerToRewardKeysRemoved();
    }
  }

  /**
   * remove all keys related to communicating offers.  Emits a result for each key removed.
   */
  public offerToRewardKeysRemoved():Observable<boolean> {
    return Observable.create((observer) => {
      this.localStorage.removeItem(offerBonusRewardsIfSignedInStorageKey).subscribe((successful) => {
        observer.next(successful);
      });
      this.localStorage.removeItem(offerBonusRewardsStorageKey).subscribe((successful) => {
        observer.next(successful);
      });
    })

  }

  /** if offerBonusRewardsStorageKey is found in the local storage matching the given user id, this will
   * return true indicating the reward can be offered.  The authenticated user could then send a scan to suggest
   * a bonus.
   *
   * @param {Tag[]} tags of the user being viewed. If any match that which is stored, this will return true.
   */
  public offerCanBeRewarded(tags: Tag[]): Observable<boolean> {
    return this.storageKeyMatchesTags(offerBonusRewardsStorageKey, tags);
  }

  /**
   *
   * @param tags of the player being scanned
   * @return true if the player can be rewarded if the scanner signs in.
   */
  public offerCanBeRewardedIfSignedIn(tags: Tag[]): Observable<boolean> {
    return this.storageKeyMatchesTags(offerBonusRewardsIfSignedInStorageKey, tags);
  }

  /**
   * Re-use of method that looks for local storage of the key given to see if the tag id matches that given.
   * @param storageKey
   * @param tags
   */
  private storageKeyMatchesTags(storageKey: string, tags: Tag[]) {
    return Observable.create((observer) => {
      return this.localStorage.getItem(storageKey).subscribe((tagId) => {
        let matched = false;
        tags.forEach((tag) => {
          if (tag && tag.id === tagId && !matched) {
            observer.next(true);
            matched = true;
          }
        });
        if(!matched){
          observer.next(false);
        }
      })
    });
  }

  /**
   * Flags local storage that the next scan received, within the next minute, will indicate the tag should be identified
   * with the user given, if the tag is not already associated with a user.  It does this by adding the expiration
   * time 1 minute from now in a local storage key.
   *
   * @param {string} userId
   * @return {Observable<boolean>}
   */
  public associateNextScanWithinOneMinuteToUser(userId: string): Observable<boolean> {
    return this.saveTagAssociation(userId);
  }

  private saveTagAssociation(userId?: string, authUid?: string): Observable<boolean> {
    const association: TagAssociation = {
      userId: userId,
      authUid: authUid,
      expires: this.gameTimeService.timestampString(this.gameTimeService.now().add(1, 'minute'))
    };
    return this.localStorage.setItem(associateTagStorageKey, association);
  }

  /**
   * When the authenticated user is choosing an avatar, but they have a school tag then this is called to associate
   * the user relatedd to the tag of the next scan with the authenticated user.
   * @param authUid
   * @see scanReceived
   */
  public associateNextScanWithinOneMinuteToAuthUser(authUid: string): Observable<boolean> {
    return this.saveTagAssociation(undefined, authUid);
  }

  /**
   * Only assigns the authId to scan if authenticated and not yet provided.
   * It will return when authentication has been deteremined or immediately if the authId is already present.
   *
   * The authenticated user is useful with every scan for reporting purposes,
   * regardless if it is required to receive a reward.
   * @return {Observable<void>}
   */
  private assignAuthUserToScanIfAuthenticated(scan: Scan) {
    return Observable.create((observer) => {
      if (scan.authUid) {
        observer.next();
      } else {
        this.firebaseAuth.authState.subscribe((authUser) => {
          if (authUser) {
            scan.authUid = authUser.uid;
          }
          observer.next();
        }, (error) => {
          observer.error(error);
        });
      }
    });
  }

  /**
   *
   * @return {Observable<TagAssociation>}
   */
  private associationForNextScan() {
    return <Observable<TagAssociation>>  Observable.create((observer) => {
      this.localStorage.getItem<TagAssociation>(associateTagStorageKey).subscribe((association) => {
        if (association && association.expires) {
          const expires = this.gameTimeService.parseTimestamp(association.expires);
          if (expires.isAfter(this.gameTimeService.now())) {
            observer.next(association);
          } else {
            console.warn(`Expired tag association found and being ignored`);
            observer.next();
          }
          //remove in any case since this method is called upon the next scan...it works only once
          this.localStorage.removeItem(associateTagStorageKey).subscribe(() => {
            //because catch syntax is confusing
          }, (error) => {
            console.warn(`unable to remove scan association to ${association.userId}`, error);
          })
        } else {
          observer.next();
        }
        observer.complete();
      });
    });
  }

  /** Parses the given tagId + counter, as provided by the Android tag URLs.
   *
   * Returns a scan with the tag id and counter, if found.  Empty scan otherwise.
   *
   * @param {string} tagIdAndCounter
   * @returns {Scan} possibly filled with tag id and counter
   */
  public static scanFromParam(tagIdAndCounter: string) {
    const scan = new Scan();
    if (tagIdAndCounter) {
      const parts = tagIdAndCounter.split('x');
      if (parts.length > 0) {
        scan.tagId = parts[0];
        if (parts.length > 1) {
          const hex = 16;
          scan.counter = Number.parseInt(parts[1], hex);
        }
      }
    }
    return scan;
  }

  /**
   * @see TagService.typeIsStation
   * @param {scan} scan
   * @returns {boolean}
   */
  public static tagIsStation(scan: Scan) {
    return scan && TagService.typeIsStation(scan.tagType)
  }

  /**
   *
   * @return {Function}
   */
  public static mostRecentScansSortFunction() {
    const leftFirst = -1;
    const rightFirst = 1;
    return (left: Scan, right: Scan) => {
      if (left) {
        if (right) {
          if (moment.parseZone(left.timestamp).isAfter(moment.parseZone(right.timestamp))) {
            return leftFirst;
          } else {
            return rightFirst;
          }
        } else {
          return leftFirst;
        }
      } else {
        //could be equal, but who cares
        return rightFirst;
      }
    }
  }


  public onDayFilterFunction(day: string) {
    return (scan: Scan) => {
      if (scan.timestamp > day) {
        const nextDay = this.gameTimeService.nextDay(day);
        if (scan.timestamp < nextDay) {
          return scan;
        }
      }
      return;
    }
  }

  /** A scan must come with an authenticated user to qualify for points.  The scan.authUid indicates who
   * authorized the scan which may result in rewards.
   *
   * @param userId
   * @param day
   * @return scans made by the given user during the given day
   *
   */
  scansByUserForDay(userId: string, day: string): Observable<Scan[]> {
    return <Observable<Scan[]>>  Observable.create((observer) => {
      this.userService.userById(userId).subscribe(user => {
        const authUid = user.authUid;
        if (authUid) {

          return this.db.list('/scans', ref => {
            return ref.orderByChild('authUid').equalTo(authUid);
          }).valueChanges().subscribe((scans) => {
            observer.next(scans.sort(ScanService.mostRecentScansSortFunction()).filter(this.onDayFilterFunction(day)));
          });
        } else {
          observer.next([]);
        }
      });
    });
  }
}
