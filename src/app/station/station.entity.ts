/**
 * A place where a player scans their tag.
 */
export class Station{

  /** desriptive unique key*/
  id:string;

  /**Title identifying the station.  May not be unique.*/
  name:string;
}
