import {Pipe, PipeTransform} from "@angular/core";
import {Station} from './station.entity';
import {StationService} from './station.service';
import {Observable} from 'rxjs';

/**
 * Provides a station given the station id.
 */
@Pipe({name: 'station'})
export class StationPipe implements PipeTransform {
  constructor(public stationService: StationService) {
  }

  transform(stationId:string): Observable<Station> {
    return  this.stationService.stationById(stationId);
  }
}
