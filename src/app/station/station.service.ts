import {Injectable} from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';
import {Station} from './station.entity';
import {Observable} from 'rxjs';

const stations = 'stations';

@Injectable()
export class StationService {

  constructor(private db: AngularFireDatabase) {

  }

  stationById(stationId: string): Observable<Station> {
    return <Observable<Station>> this.db.object(`${stations}/${stationId}`).valueChanges();

  }
}
