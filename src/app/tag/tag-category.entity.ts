/**
 * A broad generalization of how a tag is used, or the kind of tag that is providing information.
 * When combined with category, together they both provide meaning.
 * backpack tag => NFC tag for a student
 * backpack qrc => QR Code for a student
 * walk station => sticker placed to provide reward to those who scan
 * bike tag => NFC sticker on the bike
 */
export class TagType{
  /** the unique key that represents the type (tag,qrc,station, etc)*/
  public id: string;

  /** The primary image that represents this type  of tag.*/
  public imagePath: string;
}

/**
 * Information about the role of a tag.
 */
export class TagCategory{

  /** the key used to refer to a category*/
  public category:string;
  /** name of the standard icons */
  public icon:string;
  /** keyed by the tag type, provides information about the tag type.
   * @type Map<string,TagType>
   * */
  public types: Map<string,TagType>;

  /** A short description explaining more about the tag this category represents.*/
  public description:string;
}

