import {Injectable} from "@angular/core";
import {AngularFireDatabase} from "angularfire2/database";
import {TagCategory} from "./tag-category.entity";
import {Observable} from "rxjs/Observable";
import {ImageService} from "../image/image.service";
import {Tag} from "./tag.entity";
import {ImageUrl} from "../image/image-url.model";

/**
 * Provides data driven description and meta data for the various tags used. Only handles general categories and not
 * specific instances which are Tags themselves.
 */
@Injectable()
export class TagCategoryService {
  private static readonly defaultTagImagePath = '/tags/backpack-tag.png';


  constructor(private db: AngularFireDatabase,
              private imageService: ImageService) {

  }

  /** Single category matching the key.
   *
   * @param {string} category
   * @returns {Observable<TagCategory>}
   */
  public category(category: string): Observable<TagCategory> {
    return this.db.object(`tagCategories/${category}`).valueChanges();
  }


  /**
   * Given a tag, this will provide an image that represents the tag.
   *
   * @param {Tag} tag
   * @return {Observable<ImageUrl>} guaranteed to provide an image, the default image if necessary data is missing
   */
  imageForTag(tag: Tag) {

    return <Observable<ImageUrl>> Observable.create((observer) => {
      const notifyObserver = (imagePath) => {
        const imageUrl = this.imageService.withPath(imagePath);
        observer.next(imageUrl);
        observer.complete();
      }

      let imagePath = TagCategoryService.defaultTagImagePath;
      if (tag && tag.category) {
        this.category(tag.category).subscribe((tagCategoryInfo) => {
          const tagType =  tag.type ;
          if (tagCategoryInfo && tagCategoryInfo.types) {
            const tagTypeInfo = tagCategoryInfo.types[tagType];
            if (tagTypeInfo) {
              if (tagTypeInfo.imagePath) {
                imagePath = tagTypeInfo.imagePath;
              }
            }
          }
          notifyObserver(imagePath);
        });
      } else {
        notifyObserver(imagePath);
      }
    });
  }
}
