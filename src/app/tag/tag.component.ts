import {Component, OnInit} from '@angular/core';
import {TagService} from "./tag.service";
import {ScanService} from "../scan/scan.service";
import {MatDialog, MatDialogConfig, MatSnackBar} from "@angular/material";
import {ActivatedRoute, Router} from "@angular/router";
import {AppRoutes} from "../app.routes";
import {Observable} from "rxjs/Observable";
import {Scan} from "../scan/scan.entity";
import {Tag} from "./tag.entity";
import {UserService} from "../users/user.service";
import {User} from "../users/user.entity";
import {TagCategoryService} from "./tag-category.service";
import {TagCategory} from "./tag-category.entity";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {ScanDialogComponent} from "../scan/scan-dialog.component";
import {gameTimeService} from '../app.module';
import {Moment} from 'moment';

@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.css']
})
export class TagComponent implements OnInit {
  public scans: Observable<Scan[]>;
  public tag: BehaviorSubject<Tag>;
  public user: BehaviorSubject<User>;
  public tagCategory: BehaviorSubject<TagCategory>;
  public day: string;

  constructor(private tagService: TagService,
              private tagCategoryService: TagCategoryService,
              private scanService: ScanService,
              private userService: UserService,
              private route: ActivatedRoute,
              private router: Router,
              private snackBar: MatSnackBar,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    //path doesn't change within the page so a snapshot is sufficient
    const pathParamMap = this.route.snapshot.paramMap;
    const tagId = pathParamMap.get(AppRoutes.tagIdKey);
    this.day = AppRoutes.dayParamFromMap(pathParamMap);
    if (!this.day){
      this.day = gameTimeService.day(gameTimeService.now())
    }
    this.scans = this.scanService.scansForTag(tagId,this.day);

    //using behavior subjects to provide values when opening the dialog
    this.tag = new BehaviorSubject(null);
    this.tagService.tagById(tagId).subscribe(this.tag);

    this.tag.subscribe((tag) => {
      if (tag) {
        if (tag.userId) {
          this.user = new BehaviorSubject(null);
          this.userService.userById(tag.userId).subscribe(this.user);
        }
        this.tagCategory = new BehaviorSubject(null);
        this.tagCategoryService.category(tag.category).subscribe(this.tagCategory);
      }
    }, (error) => {
      console.warn('problem retrieving tag', error);
      this.missingTag();
    })
  }

  private missingTag(): void {
    this.snackBar.open('Tag is not found.', 'OK').onAction().subscribe(() => {
      AppRoutes.navigate(AppRoutes.homePath, this.router, this.snackBar);
    });
  }

  public viewUser(userId: string): void {
    AppRoutes.navigate(AppRoutes.userDayPath(userId), this.router, this.snackBar);

  }

  public viewScan(scan: Scan) {
    const config: MatDialogConfig = {
      data: {
        scan: scan
      },
      width: '350px',
    };
    this.dialog.open(ScanDialogComponent, config);
  }
}
