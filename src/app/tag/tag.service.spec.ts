import {TestBed, inject} from '@angular/core/testing';

import {TagService} from './tag.service';

describe('TagService', () => {
  describe('injection', () => {
    beforeEach(() => {
      TestBed.configureTestingModule({
        providers: [TagService]
      });
    });

    it('should be created', inject([TagService], (service: TagService) => {
      expect(service).toBeTruthy();
    }));
  });
  describe('tagIsStation', () => {
    it('should return true for exact matches',()=>{
      return expect(TagService.typeIsStation('station')).toBeTruthy();
    });
    it('should return true for upper case',()=>{
      return expect(TagService.typeIsStation('STATION')).toBeTruthy();
    });
    it('should return false for noting',()=>{
      return expect(TagService.typeIsStation()).toBeFalsy();
    });
    it('should return false for something else',()=>{
      return expect(TagService.typeIsStation('wrong')).toBeFalsy();
    });
  });
});
