import {Injectable} from '@angular/core';
import {Tag} from "./tag.entity";
import {AngularFireDatabase} from "angularfire2/database";
import {Observable} from "rxjs/Observable";


export const stationTagType = 'station'

//for path creations
const tags = 'tags'
const propertyOf = <Tag>(name: keyof Tag) => name;
const userIdProperty = propertyOf<Tag>('userId');

@Injectable()
export class TagService {

  constructor(private db: AngularFireDatabase) {

  }


  /**
   *
   * @param {string} tagId
   * @returns {Observable<Tag>}
   */
  tagById(tagId: string) {
    return <Observable<Tag>> this.db.object(`${tags}/${tagId}`).valueChanges();
  }

  /**
   *
   * @param {string} userId
   * @returns {Observable<Tag[]>}
   */
  tagsForUser(userId: string) {
    return <Observable<Tag[]>> this.db.list(tags, (ref) => {
      return ref.orderByChild(userIdProperty).equalTo(userId);
    }).valueChanges();
  }

  /**
   *
   * @param {string} [tagId]
   * @returns {string|null|undefined}
   */
  public static formatTagId(tagId: string) {
    return (tagId) ? tagId.toUpperCase() : tagId;
  }

  /**
   *
   * @param {string} tagType is tag.type, often provided during a scan of a tag
   * @returns {boolean} true if the type is exactly equal to 'station'
   */
  public static typeIsStation(tagType?: string) {
    return tagType === stationTagType || (tagType && tagType.toLowerCase() === stationTagType);
  }


}
