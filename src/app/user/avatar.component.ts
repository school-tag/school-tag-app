import {Component, OnInit} from "@angular/core";
import {AngularFireAuth} from "angularfire2/auth";
import {UserService} from "../users/user.service";
import {User as FirebaseUser} from "firebase";
import {User} from "../users/user.entity";
import {AvatarService} from "../game/avatar.service";
import {Observable} from "rxjs/Observable";
import {BehaviorSubject} from 'rxjs';
import {ScanService} from '../scan/scan.service';
import {AppRoutes} from '../app.routes';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material';


export enum PlayerType { StudentWithTag, StudentWithoutTag, NotAStudent }

@Component({
  selector: 'avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.css']
})
export class AvatarComponent implements OnInit {

  public authenticatedUser: FirebaseUser;
  public authenticatedPlayer: User;
  public notAuthenticated: boolean;
  public avatarSuggestions: BehaviorSubject<Identity[]>;

  public playerTypes = PlayerType;

  /** Choices made by the user to indicate who they are so we can walk them through a setup wizard.
   * @see AvatarComponent#playerTypeChosen
   */
  public playerTypeChosen: PlayerType;

  constructor(private angularFireAuth: AngularFireAuth,
              private userService: UserService,
              private scanService: ScanService,
              private avatarService: AvatarService,
              private router: Router,
              private snackBar: MatSnackBar
  ) {
  }

  ngOnInit() {
    this.initialize();
  }

  private initialize() {
    this.notAuthenticated = false;
    this.angularFireAuth.authState.subscribe((authResponse) => {
      if (authResponse) {
        console.log(`Logged in as ${authResponse.uid}`);
        this.authenticatedUser = authResponse;
        const authUid = this.authenticatedUser.uid;
        this.userService.userByAuthId(authUid).subscribe((user) => {
          this.authenticatedPlayer = user;
        }, () => {
          console.debug(`authenticated as ${authUid}, but no matching user found.`)
        });
      } else {
        this.notAuthenticated = true;
      }
    });
  }


  playerTypeSelected(playerType: PlayerType) {
    this.playerTypeChosen = playerType;
    console.debug(`player type chose ${playerType}`);
    switch (playerType) {
      case PlayerType.StudentWithTag:
        console.debug("student with tag chosen");
        this.associateTag();
        break;
      case PlayerType.StudentWithoutTag:
        break;
      case PlayerType.NotAStudent:
        if (!this.avatarSuggestions) {
          this.avatarSuggestions = new BehaviorSubject(null);
          this.avatarService.nonPlayerAvatarSuggestions().subscribe(this.avatarSuggestions);
        }
        break;
      default:
        throw new Error(`${playerType} not handled!`);
    }

    //scroll to show the newly provided instructions.
    //FIXME: should be in the template, not in the code
    document.getElementById("playerChoiceInstructions").scrollIntoView();
  }

  /**WIll associate the chosen identity. */
  chooseIdentity(identity: Identity) {
    const user: User = new User();
    user.avatar = identity.avatar;
    user.nickname = identity.nickname;
    user.authUid = this.authenticatedUser.uid;
    this.userService.createUser(user).then(() => {
      this.initialize();
    });
  }

  associateTag(){
    this.scanService.associateNextScanWithinOneMinuteToAuthUser(this.authenticatedUser.uid).subscribe(succeeded=>{
      if(!succeeded){
        console.error("Failed to store association");
      }else{
        console.debug("Saved the association details.")
      }
    }, error =>{
      console.error("Failed to store association",error);
    });
  }

  public signIn() {
    AppRoutes.signIn(this.router,this.snackBar);
  }
}
