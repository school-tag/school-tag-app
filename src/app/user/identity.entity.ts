/**
 * Fields used to identify a user.
 */
interface Identity {

  /**Image path relative to the other avatar images that may be used.*/
  avatar: string;
  /** groups related images together.*/
  category: string;
  /** who the identity is intended for.  observer,player.*/
  type: string;
  /** name to identity a user.*/
  nickname: string;

  /** user id already associated with this identity*/
  userId: string;
}
