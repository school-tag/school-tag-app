import {Component, Inject, OnInit} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {RuleService} from "../game/rule.service";
import {Observable} from "rxjs/Observable";
import {Rule} from "../game/rule.entity";
import {RuleIconService} from "../game/rule-icon.service";
import {RuleIconComponent} from "../game/rule-icon.component";
import {CoinsRewardIconComponent} from "../game/reward.component";

/**
 * Show the details for rewards (points or coins) given for some duration.
 */
@Component({
  selector: 'app-reward-details-dialog',
  templateUrl: './reward-details-dialog.component.html',
  styleUrls: ['./reward-details-dialog.component.css']
})
export class RewardDetailsDialogComponent implements OnInit {

  /**
   * @deprecated use RuleIconComponent.coinsRuleId;
   * @type {string}
   */
  public static readonly coinsRuleId = CoinsRewardIconComponent.coinsRuleId;

  public rule: Observable<Rule>;
  /** explains what was earned in which period */
  public earnedMessage: string;

  constructor(@Inject(MAT_DIALOG_DATA) private data: any,
              private ruleService: RuleService) {
  }

  ngOnInit() {
    const ruleId = this.data.ruleId;
    const value = this.data.value;
    const period = this.data.period;
    let rewardWord;

    if (ruleId === RewardDetailsDialogComponent.coinsRuleId) {
      const data: any = {
        event: {
          type: ruleId,
          params: {
            icon: RuleIconService.iconForCoins,
            title: 'Coins',
            description: '1 coin is earned when arriving to school before 8:10 am or by taking bus, bike or walking.',
            how: 'Check in at the School Tag Station near the MPR before morning assembly.',
            why: 'Exchange coins with prizes at the end of the competition.',
            encouragement: 'Earn more coins by having the most points on the leaderboard at the end of the game.',
            restrictions: 'Typically a single coin per day can be earned.',
          }
        }
      }
      this.rule = Observable.of(new Rule(data));
      rewardWord = (value>1)?'coins':'coin';
    } else {
      rewardWord = (value>1)?'points':'point';
      this.rule = this.ruleService.ruleById(ruleId);
    }
    this.earnedMessage = `${value} ${rewardWord} earned ${period}.`
  }
}
