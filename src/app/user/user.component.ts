import {Component, OnInit, Pipe, PipeTransform} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../users/user.service";
import {UserRewardsService} from "../game/user-rewards.service";
import {Observable} from "rxjs/Observable";
import {User} from "../users/user.entity";
import {Reward} from "../game/reward.model";
import {GameTimeService} from "../game/game-time.service";
import {ImageUrl} from "../image/image-url.model";
import {AvatarService} from "../game/avatar.service";
import {AppRoutes} from "../app.routes";
import {Location} from "@angular/common";
import {RuleIconService} from "../game/rule-icon.service";
import {Tag} from "../tag/tag.entity";
import {TagService} from "../tag/tag.service";
import {ScanService} from "../scan/scan.service";
import {MatDialog, MatDialogConfig, MatSnackBar} from "@angular/material";
import {TagCategoryService} from "../tag/tag-category.service";
import {PlayerRank} from "../leaderboard/player-rank.entity";
import {PlayerRankService} from "../leaderboard/player-rank.service";
import {RuleService} from "../game/rule.service";
import {Scan} from "../scan/scan.entity";
import {ScanDialogComponent} from "../scan/scan-dialog.component";
import {RewardDetailsDialogComponent} from "./reward-details-dialog.component";
import {BehaviorSubject} from 'rxjs';

@Pipe({name: 'tagImageUrl'})
export class TagImageUrlPipe implements PipeTransform {
  constructor(public tagCategoryService: TagCategoryService) {
  }

  transform(tag: Tag): Observable<ImageUrl> {
    return this.tagCategoryService.imageForTag(tag);
  }
}

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  /**identifies the player*/
  public nickname: string;

  /**Path to the avatar.*/
  public avatarImageUrl: ImageUrl;

  /** identifies the day being viewed.*/
  public day: string;

  /** true if the viewer has marked the player as a favorite. */
  public favorite: boolean;

  /** explains which week shown */
  public weekDisplay: string;
  /** used for nav arrow enabling */
  public nextWeekAvailable: boolean;
  /** used for nav arrow enabling */
  public nextDayAvailable: boolean;
  /** provided from the nickname*/
  public user: BehaviorSubject<User>;

  /** tags that will reward the user */
  public tags: Observable<Tag[]>;

  /** scans the user made of other tags */
  public scans: Observable<Scan[]>;

  public iconsForDay = [];
  public iconsForWeek = [];
  public iconsForGrandTotals = [];

  /** The reward object for the week, when ready. */
  public rewardsForWeek: any;
  public grandTotalRewards: any;

  /**User id from user*/
  public userId: string;
  /**The total for display*/
  public dailyTotal: number;

  public playerRank: Observable<PlayerRank>;

  /** If true, the authenticated user can suggest reward scans.*/
  public offerBonusRewards: boolean;
  /** If true, then signing in will enable reward offering.*/
  public offerBonusRewardsIfSignedIn: boolean;

  /**Call this function when date changes and it will update the daily total
   * Not observable because of the complications using arrays in the template.*/
  private dailyTotalUpdateFunction = () => {
  };


  private readonly noRewardPoints = 0;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private location: Location,
              private userService: UserService,
              private userRewardService: UserRewardsService,
              private tagService: TagService,
              private gameTimeService: GameTimeService,
              private avatarService: AvatarService,
              private playerRankService: PlayerRankService,
              private ruleIconService: RuleIconService,
              private ruleService: RuleService,
              private scanService: ScanService,
              private snackBar: MatSnackBar,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      // nickname or user id matches select path variable
      this.userId = AppRoutes.userKeyParam(params);
      const dayParam = AppRoutes.dayParam(params);
      //don't offer rewards, unless the scan service says to.

      let day = this.gameTimeService.dayFromParam(dayParam);
      this.setDay(day);
      this.user = new BehaviorSubject(null);
      this.userService.userById(this.userId).subscribe(this.user);
      this.tags = this.tagService.tagsForUser(this.userId);
      this.playerRank = this.playerRankService.gameTotalPlayerRankForUser(this.userId);
      this.user.subscribe((user) => {
        if (user) {
          this.userId = user.id;
          this.nickname = user.nickname;
          this.avatarImageUrl = this.avatarService.imageUrl(user.avatar);
          this.fetchRewards();
          return user;
        }
      });

      this.userService.isFavorite(this.userId).subscribe((isFavorite) => {
        this.favorite = isFavorite;
      });

      this.tags.subscribe((tags) => {
        //bonus rewards are offered if a scan was read of a tag by an authenticated user
        this.scanService.offerCanBeRewarded(tags).subscribe((canBeRewarded) => {
          this.offerBonusRewards = canBeRewarded;
          this.scanService.offerCanBeRewardedIfSignedIn(tags).subscribe((canBeRewarded) => {
            // this.offerBonusRewardsIfSignedIn = canBeRewarded;
            //disabled to since rewards aren't given
            this.offerBonusRewardsIfSignedIn = false;
          });
        });

      })
    });
  }

  /**Assigns the day and updates all that is related to a day change
   * @param {string} day
   */
  private setDay(day: string) {
    //get the week displayed, if any before assigning
    const previousWeekDisplayed = this.day ? this.gameTimeService.weekFromDay(this.day) : -1;
    this.day = day;

    //update the browser address
    this.location.go(AppRoutes.userDayPath(this.userId, day));

    //setup day display
    this.nextDayAvailable = this.gameTimeService.nextDayIsAvailable(this.day);

    //scans are shown for a day
    this.scans = this.scanService.scansByUserForDay(this.userId, this.day);


    //setup week display
    this.nextWeekAvailable = this.gameTimeService.nextWeekIsAvailable(this.day);
    this.weekDisplay = this.gameTimeService.weekDisplay(day);

    if (this.gameTimeService.weekFromDay(this.day) !== previousWeekDisplayed || !this.dailyTotalUpdateFunction) {
      this.fetchRewards();
    } else {
      this.dailyTotalUpdateFunction();
    }

  }

  today() {
    this.setDay(this.gameTimeService.mostRecentDay());
  }

  nextDay() {
    this.setDay(this.gameTimeService.nextDay(this.day));
  }

  previousDay() {
    this.setDay(this.gameTimeService.previousDay(this.day));
  }

  nextWeek() {
    this.setDay(this.gameTimeService.sameWeekdayNextWeek(this.day));
  }

  previousWeek() {
    this.setDay(this.gameTimeService.sameWeekdayPreviousWeek(this.day));
  }

  private fetchRewards() {
    if (this.userId) {
      const date = this.gameTimeService.parseDay(this.day);
      let year = this.gameTimeService.year(date);
      let week = this.gameTimeService.week(date);
      let dayOfWeek = this.gameTimeService.dayOfWeek(date);
      this.fetchDayTotals(year, week, dayOfWeek);
      this.fetchWeekTotals(year, week);
      this.fetchGrandTotals(year);
    }
  }

  private fetchDayTotals(year: number, week: number, dayOfWeek: number) {
    this.userRewardService.scanRewardsForDay(this.userId, year, week, dayOfWeek);
    this.userRewardService.dailyTotalsForWeek(this.userId, year, week).subscribe((rewards) => {
      this.dailyTotalUpdateFunction = () => {
        this.iconsForDay = [];
        if (this.day && rewards) {
          const date = this.gameTimeService.parseDay(this.day);
          let reward = rewards[date.isoWeekday()];
          if (reward) {
            const rules = reward.pointsForRules;
            const iconArray = this.iconsForDay;
            this.displayRuleIcons(rules, iconArray);
            this.displayCoinIcons(reward, iconArray);
          }
          this.dailyTotal = reward ? reward.points : this.noRewardPoints;
        } else {
          this.dailyTotal = this.noRewardPoints;
        }
      };
      this.dailyTotalUpdateFunction();
    })
  }

  private fetchWeekTotals(year: number, week: number) {
    this.userRewardService.weekTotals(this.userId, year, week).subscribe((reward) => {
      this.iconsForWeek = [];
      if (reward) {
        this.rewardsForWeek = reward;
        this.displayRuleIcons(this.rewardsForWeek.pointsForRules, this.iconsForWeek);
        this.displayCoinIcons(this.rewardsForWeek, this.iconsForWeek);
      } else {
        this.rewardsForWeek = new Reward();
        this.rewardsForWeek.points = this.noRewardPoints;
      }
    });
  }


  private fetchGrandTotals(year: number) {
    //only have yearlies right now, but that will do
    this.userRewardService.yearTotals(this.userId, year).subscribe((reward) => {
      this.iconsForGrandTotals = [];
      if (reward) {
        this.grandTotalRewards = reward;
        this.displayRuleIcons(this.grandTotalRewards.pointsForRules, this.iconsForGrandTotals);
        this.displayCoinIcons(this.grandTotalRewards, this.iconsForGrandTotals);
      } else {
        this.grandTotalRewards = new Reward();
        this.grandTotalRewards.points = this.noRewardPoints;
      }
    });
  }

  /**
   * used to report the iconName and associated value to go with it:
   * For Rules, the points.  For coins, the number of coins.
   * @param {string} ruleId identifying the reason for the reward
   * @param {string} iconName the corresponding material icon name
   * @param {number} value - the number to be shown corresponding to the icon
   * @returns {{iconName: string; value: number, title: string}}
   */
  private iconValue(ruleId: string, iconName: string, value: number) {
    return {
      ruleId: ruleId,
      iconName: iconName,
      value: value,
    }
  }

  private displayCoinIcons(reward: Reward, iconArray: Array<object>) {
    if (reward && reward.coins && reward.coins > 0) {
      const iconValue = this.iconValue(RewardDetailsDialogComponent.coinsRuleId, RuleIconService.iconForCoins, reward.coins);
      iconArray.push(iconValue);
    }
  }

  /**
   * An overly complicated way of grabbing the getting the icons ready to be displayed with their group.
   * @param {Object} pointsForRules
   * @param {Array<object>} iconArray
   */
  private displayRuleIcons(pointsForRules: Object, iconArray: Array<object>) {
    if (pointsForRules) {
      Object.keys(pointsForRules).forEach((ruleId) => {
        const points = pointsForRules[ruleId];
        const iconValue = this.iconValue(ruleId, this.ruleIconService.iconForRule(ruleId), points);
        iconArray.push(iconValue);
      })
    }
  }


  /**
   * Navigates to the current school day being viewed to see all those participating.
   */
  public viewSchoolDay() {
    this.router.navigateByUrl(AppRoutes.daySelector(this.day)).catch((error) => {
      console.error(error);
    });
  }

  public viewTag(tagId: string) {
    AppRoutes.navigate(AppRoutes.tag(tagId,this.day), this.router, this.snackBar);
  }

  /**Shows the scan dialog explaining the details of a scan.*/
  public viewScan(scan: Scan) {
    const config: MatDialogConfig = {
      data: {
        scan: scan
      },
      width: '350px',
    };
    this.dialog.open(ScanDialogComponent, config);
  }

  public addTag() {
    if (this.userId) {
      this.scanService.associateNextScanWithinOneMinuteToUser(this.userId).subscribe((stored) => {
        if (stored) {
          this.snackBar.open(`Scan a tag within the next minute to link it to ${this.nickname}`, 'OK', {
            duration: 3000
          });
        } else {
          this.snackBar.open(`Failed to setup for tag association.`, 'OK', {
            duration: 3000
          });
        }
      }, (error) => {
        this.snackBar.open(`Associating a tag is not possible in this browser.`, 'OK', {
          duration: 3000
        });

      });
    }
  }

  /**
   *
   * @param iconValue the icon value being clicked
   * @param period displayed to the user explaining the time period when the reward was given
   */
  public viewRewardDetails(iconValue, period: string) {
    const config: MatDialogConfig = {
      data: {
        ruleId: iconValue.ruleId,
        value: iconValue.value,
        period: period,
      },
      width: '350px',
    };
    this.dialog.open(RewardDetailsDialogComponent, config);
  }

  /**
   * Pressed by authenticated user, after scanning a player, to reward a player for a specific reason.
   *
   * @param {string} ruleId
   */
  public sendRewardScan(ruleId: string): void {
    this.tags.subscribe((tags) => {
      if (tags.length > 0) {
        const scan = new Scan();
        const firstTag = tags[0];
        scan.tagId = firstTag.id;
        scan.category = ruleId;
        this.scanService.scanReceived(scan).subscribe(() => {
          this.snackBar.open(`Sent a request to reward ${this.nickname}.`, 'OK', {
            duration: 3000
          })
        }, (error) => {
          console.error(error);
          this.snackBar.open(`Failure to send reward. Please try again`, 'OK')
        });
        this.scanService.offerToRewardKeysRemoved().subscribe((successful) => {
          if (!successful) {
            console.error("failure to remove offer key");
          }
        });
      }
    })
  }

  /**Shortcut offered when a scan is made
   * @see offerBonusRewardsIfSignedIn
   */
  public signIn() {
    //the scan must take place again to offer up the message
    this.scanService.offerToRewardKeysRemoved().subscribe((successful) => {
      if (!successful) {
        console.error("failed to remove an offer key");
      }
    });
    AppRoutes.signIn(this.router, this.snackBar);
  }


  public markAsFavorite() {
    this.userService.markAsFavorite(this.userId).subscribe((saved) => {
      let message;
      if (saved) {
        message = `Marked ${this.nickname} as a favorite.`;
        this.favorite = true;
      } else {
        message = "Not saved.";
      }
      this.snackBar.open(message, 'OK', {
        duration: 3000
      });
    })
  }

  public removeAsFavorite() {
    this.userService.removeAsFavorite(this.userId).subscribe((removed) => {
      let message;
      if (removed) {
        message = `Removed ${this.nickname} as a favorite.`;
        this.favorite = false;
      } else {
        message = "Failed to remove favorite. Try again?";
        this.favorite = true;
      }
      this.snackBar.open(message, 'OK', {
        duration: 3000
      });
    });
  }
}


