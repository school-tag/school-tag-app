/**
 * Entity representing any actor on the system.
 *
 *
 */
import {ImageUrl} from "../image/image-url.model";

export class User {
  /**
   * the same as $key, but comes as part of the entity.
   */
  id: string;
  /** a common name that identifies the user. */
  nickname: string;
  /** the key from the parent reference. */
  $key: string;
  /** relative path to the avatar that represents this user.*/
  avatar: string;
  /** absolute path to find the image for the avatar */
  avatarUrl: ImageUrl;
  /** Firebae auth uid, if the user has an associated login.*/
  authUid:string;
}
