import {AngularFireDatabase} from 'angularfire2/database';
import {Injectable} from '@angular/core';
import {User} from "./user.entity";
import {Observable} from "rxjs/Observable";
import {AvatarService} from "../game/avatar.service";
import {ImageUrl} from "../image/image-url.model";
import {AsyncLocalStorage} from 'angular-async-local-storage';
import {GameTimeService} from '../game/game-time.service';
import {BehaviorSubject} from 'rxjs';


/**
 * Retrieval and modifications of the user entity which represents any actor (usually a person,
 * sometimes a machine).
 */
@Injectable()
export class UserService {

  private readonly favoritesKey = "favorites";
  private favoritesStream = new BehaviorSubject({});

  constructor(private db: AngularFireDatabase,
              private localStorage: AsyncLocalStorage,
              private gameTimeService: GameTimeService) {
    this.localStorage.getItem(this.favoritesKey).subscribe((favorites) => {
      this.favoritesStream.next(favorites ? favorites : {});
    });
  }


  /**
   *
   * @return {Observable<User[]>} list of users matching the filter ordered alphabetically by nickname
   */
  public users() {
    return <Observable<User[]>> this.db.list('/users', (ref) => {
      return ref.orderByChild('nickname');
    }).valueChanges();
  }

  /**
   * @param {string} nickname
   * @returns {Observable<User[]>}
   */
  public usersForNickname(nickname: string) {
    console.log('searching for nickname', nickname);
    return this.db.list('/users', (ref) => {
      return ref.orderByChild('nickname').equalTo(nickname);
    }).valueChanges();
  }

  /**
   * @param {string} userId
   * @returns {Observable<User>}
   */
  public userById(userId: string): Observable<User> {
    return <Observable<User>> this.db.object(`/users/${userId}`).valueChanges();
  }

  public usersByIds(userIds: string[]): Observable<User[]> {
    return Observable.create((observer) => {
      const users = [];
      observer.next(users);
      userIds.forEach((userId) => {
        this.userById(userId).subscribe((user) => {
          users.push(user);
        });
      });
    });
  }

  /**
   * @param {User} user
   * @return {firebase.database.ThenableReference}
   */
  public createUser(user: User) {
    const thenableReference = this.db.list('users').push(user);
    //user.id must have the newly created key...it would be better for the cloud to do this
    const userId = thenableReference.key;
    return thenableReference.then(() => {
      return this.db.object(`/users/${userId}/id`).set(userId);
    });
  }

  /**
   *
   * @param {string} authId
   * @returns {Observable<User>}
   */
  public userByAuthId(authId: string) {
    return <Observable<User>> Observable.create((observer) => {
      this.db.list('users', (ref) => {
        return ref.orderByChild('authUid').equalTo(authId).limitToFirst(1);
      }).valueChanges().subscribe((users) => {
        if (!users || users.length == 0) {
          observer.next(null);
        } else {
          const user = <User> users[0];
          observer.next(user);
        }
      });
    });
  }

  /**
   * Writes the authUid component to a user if and only if the authUid is not already assigned, which is enforced by the
   * server.
   * @param authUid
   * @param userId
   */
  assignAuthUidToUser(authUid: string, userId: string): Observable<void> {
    return <Observable<void>> Observable.create((observer) => {
      const user: User = <User>{
        authUid: authUid
      };
      this.db.object(`/users/${userId}`).update(user)
        .then(() => {
          observer.next();
        })
        .catch(error => {
          observer.error(error);
        })
    })
  }

  /**
   *
   * @param userId
   */
  public isFavorite(userId: string) {
    return Observable.create((observer) => {
      this.favorites().subscribe((favorites) => {
        let result = false;
        if (favorites) {
          if (favorites[userId]) {
            result = true;
          }
        }
        observer.next(result);
      });
    })
  }


  /**
   * Marks the user given as a favorite.  Currently limited to the local device not associated to the user.
   * @param userId
   * @return true if successfully stored
   */
  public markAsFavorite(userId: string): Observable<boolean> {
    const favorites = this.favoritesStream.getValue();
    favorites[userId] = this.gameTimeService.now().format();
    return this.saveFavorites(favorites);
  }

  /**Retrieves the map of userId -> timestamps that have been marked as favorites.
   *
   */
  public favorites(): Observable<any> {
    return this.favoritesStream;
  }

  public removeAsFavorite(userId: string): Observable<boolean> {
    const favorites = this.favoritesStream.getValue();
    delete favorites[userId];
    return this.saveFavorites(favorites);
  }

  private saveFavorites(favorites): Observable<boolean> {
    return Observable.create((observer)=>{
      this.localStorage.setItem(this.favoritesKey, favorites).subscribe((saved) => {
        //the stream is listening to the storage apparently.
        if (saved) {
          this.favoritesStream.next(favorites);
          observer.next(true);
        } else {
          observer.next(false);
        }
      }, (error) => {
        console.error(error);
        observer.next(false);
      });
    });

  }
}


export enum UserError {
  NicknameNotFound
}
