import {Component, OnInit} from '@angular/core';
import {UserService} from "./user.service";
import {Observable} from "rxjs/Observable";
import {User} from "./user.entity";
import {Router} from "@angular/router";
import {AppRoutes} from "../app.routes";
import {MatSnackBar} from "@angular/material";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  public static readonly selector = 'users';
  public users: Observable<User[]>;

  constructor(private userService: UserService,
              private router: Router,
              private snackBar:MatSnackBar) {
  }

  ngOnInit() {
    this.users = this.userService.users();
  }


  public viewUser(user: User) {
    AppRoutes.navigate(AppRoutes.userDayPath(user.id),this.router,this.snackBar);
  }
}
