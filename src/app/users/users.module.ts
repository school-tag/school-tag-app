import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {TagImageUrlPipe, UserComponent} from '../user/user.component';
import {UsersComponent} from './users.component';
import {MaterialModule} from "../material.module";
import {MomentModule} from "angular2-moment";
import { TagComponent } from '../tag/tag.component';
import {UserAvatarImageUrlPipe} from "../game/user-avatar-image-url.pipe";
import {AuthUidAvatarImageUrlPipe} from "../game/auth-user-avatar-image-url.pipe";
import {ScanDialogComponent} from "../scan/scan-dialog.component";
import {GameWeekPipe} from "../game/game-week.pipe";
import {DaysModule} from "../days/days.module";
import {GameDayTimePipe} from "../game/game-day-time.pipe";
import {EntriesPipe} from "../util/object.pipe";
import {RulesModule} from "../rules/rules.module";
import {RewardDetailsDialogComponent} from "../user/reward-details-dialog.component";
import {AvatarComponent} from "../user/avatar.component";
import {StationPipe} from '../station/station.pipe';

@NgModule({
  imports: [
    CommonModule,
    DaysModule,
    MaterialModule,
    MomentModule,
    RulesModule,
  ],
  declarations: [
    AuthUidAvatarImageUrlPipe,
    AvatarComponent,
    EntriesPipe,
    GameDayTimePipe,
    GameWeekPipe,
    RewardDetailsDialogComponent,
    ScanDialogComponent,
    UserAvatarImageUrlPipe,
    StationPipe,
    TagImageUrlPipe,
    TagComponent,
    UserComponent,
    UsersComponent,
  ],
  entryComponents: [
    RewardDetailsDialogComponent,
    ScanDialogComponent,
  ],
  exports: [
    AuthUidAvatarImageUrlPipe,
    EntriesPipe,
    GameDayTimePipe,
    GameWeekPipe,
    StationPipe,
    TagImageUrlPipe,
    UserAvatarImageUrlPipe,
  ]
})
export class UsersModule {
}
