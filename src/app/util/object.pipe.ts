import {Pipe, PipeTransform} from "@angular/core";


export class Entry<T>{
  public key:string;
  public value:T;
}
/**
 * Converts an object into an array of key/value "entry" objects.
 *
 * users =
 * {
 *  "bob":{
 *    city: "San Francisco"
 *  },
 *  "john":{
 *    city: "New York"
 *  }
 * }
 *
 * *ngFor"let userEntry of users | entries"
 * userEntry.key == "bob"
 * userEntry.value.city == "San Francisco"
 */
@Pipe({name: 'entries'})
export class EntriesPipe<T> implements PipeTransform {
  transform(obj) : Entry<T>[] {
    let entries = [];
    for (let key in obj) {
      const entry:Entry<T> = {
        key: key,
        value: obj[key]
      }
      entries.push(entry);
    }
    return entries;
  }
}
