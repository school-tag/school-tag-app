import {Component, OnDestroy, OnInit, Renderer, Renderer2} from '@angular/core';
import {GameState, GameTimeService} from '../game/game-time.service';
import {MessageService} from '../message/message.service';
import {Observable} from 'rxjs';
import {Message} from '../message/message.entity';
import {User} from '../users/user.entity';
import {Reward} from '../game/reward.model';
import {UserRewardsService} from '../game/user-rewards.service';
import {forEach} from '@angular/router/src/utils/collection';


@Component({
  selector: 'app-wallboard',
  templateUrl: './wallboard.component.html',
  styleUrls: ['./wallboard.component.css']
})
export class WallboardComponent implements OnInit, OnDestroy {

  //hack to allow iterating of object keys. https://stackoverflow.com/a/45880507/721000
  Object = Object;

  public gameState: GameState;
  public userViewing: User;
  public dayTotalsForUserViewing: Observable<Reward>;
  public weekTotalsForUserViewing: Observable<Reward>;
  public yearTotalsForUserViewing: Observable<Reward>;

  constructor(private gameTimeService: GameTimeService,
              private messageService: MessageService,
              private userRewardsService: UserRewardsService,
              private renderer: Renderer2
  ) {
  }

  ngOnDestroy(): void {
  }

  ngOnInit(): void {
    this.gameStateUpdated(this.gameTimeService.gameState());
    this.updateMessages();
  }

  private setBackgroundColor(color: string) {
    this.renderer.setStyle(document.body, 'background-color', color);//using .css file is ignored:(
  }

  private updateMessages() {
    const messages = this.messageService.mostRecentMessages(10);
    messages.subscribe((messages) => {
      //find the most recent message that was scanned next to the wallboard since that is the viewer
      //only show the scan for a short period. the messages are updated with the game timer refresh
      const staleTime = this.gameTimeService.now().subtract(25, 'seconds');
      let found = false;
      let i = 0;
      while (i < messages.length && !found) {
        const message = messages[i];
        //hacky, but the message at zero meters to school means they are looking at the wallboard
        if (message.aboutPayload.metersToSchool == 0) {
          const moment = this.gameTimeService.parseTimestamp(message.timestamp);
          if (moment.isBefore(staleTime)) {
            found = true;
            //clear out the user details
            this.userViewing = undefined;
          } else {
            //only refresh user if different user found
            if (!this.userViewing || this.userViewing.id != message.aboutUser.id) {
              this.userViewing = message.aboutUser;
              this.dayTotalsForUserViewing = this.userRewardsService.totalsForDay(this.userViewing.id);
              this.weekTotalsForUserViewing = this.userRewardsService.thisWeekTotals(this.userViewing.id);
              this.yearTotalsForUserViewing = this.userRewardsService.thisYearTotals(this.userViewing.id);
            }
            found = true;
          }
        }
        i++;
      }
    })
  }

  /**
   * assigns css to the body to flash the background for complete awareness that the game is ending/ended.
   */
  addAlertStyle(pulseClass: string) {
    this.renderer.setStyle(document.body, 'animation', `${pulseClass} 5s 100`);//pulse for about 10 minutes
  }

  /**
   * called by the component event.  see html
   * @param gameState
   */
  gameStateUpdated(gameState: GameState) {
    this.gameState = gameState;
    this.gameEndingSoon(gameState.nearTheEnd);
    this.gameEnded(gameState.past);
    this.gameStarted(gameState.active);
    if (this.gameState.notPlaying) {
      this.setBackgroundColor(`black`);
    } else {
      this.setBackgroundColor(`#36454f`);
    }
  }

  gameEnded(ended: boolean) {
    if (ended) {
      this.addAlertStyle('game-is-over');
    }
  }

  gameEndingSoon(bellRang: boolean) {
    if (bellRang) {
      this.addAlertStyle('game-is-ending');
    }
  }

  gameStarted(started: boolean) {
    if (started) {
      this.updateMessages();
    }
  }


  fullScreen() {
    let elem = document.documentElement;
    let methodToBeInvoked = elem.requestFullscreen ||
      elem.webkitRequestFullScreen || elem['mozRequestFullscreen']
      ||
      elem['msRequestFullscreen'];
    if (methodToBeInvoked) methodToBeInvoked.call(elem);
  }
}
