import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {WallboardComponent} from './wallboard.component';
import {
  MatCardModule,
  MatGridListModule,
  MatIconModule,
  MatProgressBarModule,
  MatToolbarModule
} from '@angular/material';
import {UsersModule} from '../users/users.module';
import {RulesModule} from '../rules/rules.module';
import {DaysModule} from '../days/days.module';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatGridListModule,
    MatIconModule,
    MatToolbarModule,
    MatProgressBarModule,
    RulesModule,
    DaysModule,
    UsersModule,
  ],
  declarations:[
    WallboardComponent,
  ],
  exports: [
  ],

  bootstrap: [WallboardComponent],

})
export class WallboardModule{
}
