// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: true,
  envName: 'playground',
  firebase: {
    apiKey: 'AIzaSyCLnNTK42BeYGRl1lo9d9EcBxDm9z_j7Dw',
    authDomain: 'school-tag-playground.firebaseapp.com',
    databaseURL: 'https://school-tag-playground.firebaseio.com',
    projectId: 'school-tag-playground',
    storageBucket: 'school-tag-playground.appspot.com',
    messagingSenderId: '61930490168'
  }
};
