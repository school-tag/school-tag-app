// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyD4ZqRPQioYSuNmkui1kAXky1eiDr8lPew",
    authDomain: "school-tag-sbox.firebaseapp.com",
    databaseURL: "https://school-tag-sbox.firebaseio.com",
    projectId: "school-tag-sbox",
    storageBucket: "school-tag-sbox.appspot.com",
    messagingSenderId: "987279496712"  }
};
