
//https://angular.io/guide/testing#create-an-observable-test-double

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { convertToParamMap, ParamMap } from '@angular/router';
import {Injectable} from "@angular/core";

@Injectable()
export class ActivatedRouteStub {

  // ActivatedRoute.paramMap is Observable
  private paramSubject = new BehaviorSubject(convertToParamMap(this.testParamMap));
  paramMap = this.paramSubject.asObservable();

  private queryParamSubject = new BehaviorSubject(convertToParamMap(this.testQueryParamMap));
  queryParamMap = this.queryParamSubject.asObservable();

  // Test parameters
  private _testParamMap: ParamMap;
  get testParamMap() { return this._testParamMap; }
  set testParamMap(params: {}) {
    this._testParamMap = convertToParamMap(params);
    this.paramSubject.next(this._testParamMap);
  }

  // Test query parameters
  private _testQueryParamMap: ParamMap;
  get testQueryParamMap() { return this._testQueryParamMap; }
  set testQueryParamMap(params: {}) {
    this._testQueryParamMap = convertToParamMap(params);
    this.queryParamSubject.next(this._testQueryParamMap);
  }

  // ActivatedRoute.snapshot.paramMap
  get snapshot() {
    return { paramMap: this.testParamMap,
    queryParamMap: this.testQueryParamMap};
  }
}
